-module(kafka_consumer_group_SUITE).
-include_lib("common_test/include/ct.hrl").
-include_lib("brod/include/brod.hrl").

-compile(export_all).

all() ->
    [
        {group, main}
    ].

groups() ->
    [
        {main, [sequence], [
            %% start_stop_group,
            handle_error_qt
        ]}
    ].


init_per_suite(Config) ->
    {ok, _A} = application:ensure_all_started(kafka_consumer_group),
    {ok, _B} = application:ensure_all_started(meck),
    {ok, _C} = application:ensure_all_started(msgpack),
    {ok, _D} = application:ensure_all_started(hash),
    _ = lager_common_test_backend:bounce(debug),
    Config.


end_per_suite(Config) ->
    Config.


start_stop_group(_) ->
    {ok, _} = start_consumer_group(),
    ok = kafka_consumer_group:stop_group(<<"test_consumer_group">>).


handle_error_qt(_) ->
    {ok, _} = start_consumer_group(),
    ok = brod:produce_sync(
        kafka_client_1,
        <<"test_topic">>,
        0,
        <<"a">>,
        msgpack:pack(#{foo => erlang:system_time(millisecond)})
    ),
    receive
        {test_message, L} ->
            _ = lager:debug(
                "Received test message, topic=~p, partition=~p, offset=~p, "
                "key=~p, value=~p",
                L
            )
    after 5000 ->
        error(no_messages)
    end,
    ok = kafka_consumer_group:stop_group(<<"test_consumer_group">>).



%% =============================================================================
%% CALLBACKS
%% =============================================================================



init(Map) ->
    {ok, Map}.


handle_message(Topic, Part, M, _Subs, #{pid := Pid} = St) ->
    #kafka_message{
        offset = Offset,
        magic_byte = _,
        attributes = _,
        key = Key,
        value = Value,
        crc = _,
        ts_type = _,
        ts = _
    } = M,

    L = [Topic, Part, Offset, Key, msgpack:unpack(Value)],
    Pid ! {test_message, L},
    {error, test, St}.



%% =============================================================================
%% UTILS
%% =============================================================================



start_consumer_group() ->
    kafka_consumer_group:start_group(#{
        client_id => kafka_client_1,
        topics => [
            #{
                name => <<"test_topic">>,
                max_retries => 3,
                quarantine_topic_name => <<"test_quarantine_topic">>,
                quarantine_message_formatter => fun
                        (GroupId, Topic, M, Reason) ->
                            #kafka_message{
                                key = Key,
                                value = Value,
                                offset = Offset
                            } = M,
                            {
                                fun(_Topic, PartCount, _Key, _Value) ->
                                    {ok, hash:fnv32a(GroupId) rem PartCount}
                                end,
                                Key,
                                msgpack:pack(#{
                                    offset => Offset,
                                    message => Value,
                                    group_id => GroupId,
                                    topic => Topic,
                                    reason => Reason
                                })
                            }
                    end
            }
        ],
        group_id => <<"test_consumer_group">>,
        callback_mod => ?MODULE,
        callback_mod_args => #{pid => self()},
        consumer_opts => #{
            begin_offset => earliest
        }
    }).

