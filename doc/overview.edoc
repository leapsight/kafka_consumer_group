%%    Copyright 2017 Ngineo Limited t/a Leapsight
%%
%%    Licensed under the Apache License, Version 2.0 (the "License");
%%    you may not use this file except in compliance with the License.
%%    You may obtain a copy of the License at
%%
%%        http://www.apache.org/licenses/LICENSE-2.0
%%
%%    Unless required by applicable law or agreed to in writing, software
%%    distributed under the License is distributed on an "AS IS" BASIS,
%%    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%%    See the License for the specific language governing permissions and
%%    limitations under the License.

@copyright 2017 Ngineo Limited t/a Leapsight
@version 0.1
@title kafka_consumer_group

@doc

## Kafka Consumer Group (KCG)
An erlang app which is a brod (kafka client) wrapper implementing a consumer group where every topic-partition is handled by its own process.

Specifically, KCG has The following characteristics:

* A single function to start a group (see {@link kafka_consumer_group:start_group/1}), no additional configuration is required.
* Every group has N associated topics
* Every group has its own supervision tree (see {@link kafka_consumer_group_sup}) under the root supervisor.
* One erlang process (see {@link kafka_group_supervisor}) per topic partition in the group
* The application crashes every time brod cannot get a connection with Kafka after a number of retries.
* Support for Quarantine topics (Dead-letter topic) per topic
* Overload support, pausing consumption using an exponential backoff strategy


