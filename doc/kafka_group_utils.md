

# Module kafka_group_utils #
* [Function Index](#index)
* [Function Details](#functions)

<a name="index"></a>

## Function Index ##


<table width="100%" border="1" cellspacing="0" cellpadding="2" summary="function index"><tr><td valign="top"><a href="#get_all_client_configs-0">get_all_client_configs/0</a></td><td></td></tr><tr><td valign="top"><a href="#get_client_config-1">get_client_config/1</a></td><td>
Returns {Endpoints, Name, Config}.</td></tr><tr><td valign="top"><a href="#validate_client_opts-1">validate_client_opts/1</a></td><td></td></tr><tr><td valign="top"><a href="#validate_consumer_opts-1">validate_consumer_opts/1</a></td><td></td></tr><tr><td valign="top"><a href="#validate_group_opts-1">validate_group_opts/1</a></td><td></td></tr><tr><td valign="top"><a href="#validate_producer_opts-1">validate_producer_opts/1</a></td><td></td></tr></table>


<a name="functions"></a>

## Function Details ##

<a name="get_all_client_configs-0"></a>

### get_all_client_configs/0 ###

`get_all_client_configs() -> any()`

<a name="get_client_config-1"></a>

### get_client_config/1 ###

`get_client_config(ClientId) -> any()`

Returns {Endpoints, Name, Config}

<a name="validate_client_opts-1"></a>

### validate_client_opts/1 ###

<pre><code>
validate_client_opts(Opts::#{}) -&gt; [{atom(), any()}] | no_return()
</code></pre>
<br />

__See also:__ [for details about producer config](brod_producer.md#start_link-4).

<a name="validate_consumer_opts-1"></a>

### validate_consumer_opts/1 ###

<pre><code>
validate_consumer_opts(Opts::#{}) -&gt; [{atom(), any()}] | no_return()
</code></pre>
<br />

<a name="validate_group_opts-1"></a>

### validate_group_opts/1 ###

<pre><code>
validate_group_opts(Opts::#{}) -&gt; [{atom(), any()}] | no_return()
</code></pre>
<br />

<a name="validate_producer_opts-1"></a>

### validate_producer_opts/1 ###

<pre><code>
validate_producer_opts(Opts::#{}) -&gt; [{atom(), any()}] | no_return()
</code></pre>
<br />

