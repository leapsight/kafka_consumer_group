

# Module kafka_group_subscriber_sup #
* [Function Index](#index)
* [Function Details](#functions)

__Behaviours:__ [`supervisor`](supervisor.md).

<a name="index"></a>

## Function Index ##


<table width="100%" border="1" cellspacing="0" cellpadding="2" summary="function index"><tr><td valign="top"><a href="#add_child-1">add_child/1</a></td><td></td></tr><tr><td valign="top"><a href="#init-1">init/1</a></td><td></td></tr><tr><td valign="top"><a href="#start_link-1">start_link/1</a></td><td>
Starts a new supervisor instance.</td></tr><tr><td valign="top"><a href="#stop-1">stop/1</a></td><td></td></tr></table>


<a name="functions"></a>

## Function Details ##

<a name="add_child-1"></a>

### add_child/1 ###

<pre><code>
add_child(Opts::<a href="kafka_group_subscriber.md#type-opts">kafka_group_subscriber:opts()</a>) -&gt; {ok, pid()} | {error, any()}
</code></pre>
<br />

<a name="init-1"></a>

### init/1 ###

`init(X1) -> any()`

<a name="start_link-1"></a>

### start_link/1 ###

<pre><code>
start_link(GroupId::binary()) -&gt; {ok, pid()} | ignore | {error, {already_started, pid()} | {shutdown, term()} | term()}
</code></pre>
<br />

Starts a new supervisor instance

<a name="stop-1"></a>

### stop/1 ###

<pre><code>
stop(GroupId::atom()) -&gt; ok | {error, not_found}
</code></pre>
<br />

