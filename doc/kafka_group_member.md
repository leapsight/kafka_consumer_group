

# Module kafka_group_member #
* [Description](#description)
* [Data Types](#types)
* [Function Index](#index)
* [Function Details](#functions)

This module represents a Kafka Consumer Group member.

__Behaviours:__ [`brod_group_member`](brod_group_member.md), [`gen_server`](gen_server.md).

<a name="description"></a>

## Description ##
Typically we will have
one of these per erlang node per GroupId.
It implements brod_group_member and sets up one subscriber (worker) per
topic per partition (as it allows to setup a group with multiple topics)
<a name="types"></a>

## Data Types ##




### <a name="type-opts">opts()</a> ###


<pre><code>
opts() = #{client_id =&gt; atom(), group_id =&gt; binary(), topics =&gt; [<a href="kafka_consumer_group.md#type-topic_spec">kafka_consumer_group:topic_spec()</a>], callback_mod =&gt; module(), callback_mod_args =&gt; any(), consumer_opts =&gt; #{}, producer_opts =&gt; #{}, group_opts =&gt; #{}}
</code></pre>

<a name="index"></a>

## Function Index ##


<table width="100%" border="1" cellspacing="0" cellpadding="2" summary="function index"><tr><td valign="top"><a href="#assign_partitions-3">assign_partitions/3</a></td><td></td></tr><tr><td valign="top"><a href="#assignments_received-4">assignments_received/4</a></td><td></td></tr><tr><td valign="top"><a href="#assignments_revoked-1">assignments_revoked/1</a></td><td></td></tr><tr><td valign="top"><a href="#code_change-3">code_change/3</a></td><td></td></tr><tr><td valign="top"><a href="#get_committed_offsets-2">get_committed_offsets/2</a></td><td></td></tr><tr><td valign="top"><a href="#handle_call-3">handle_call/3</a></td><td></td></tr><tr><td valign="top"><a href="#handle_cast-2">handle_cast/2</a></td><td></td></tr><tr><td valign="top"><a href="#handle_info-2">handle_info/2</a></td><td></td></tr><tr><td valign="top"><a href="#init-1">init/1</a></td><td></td></tr><tr><td valign="top"><a href="#start_link-1">start_link/1</a></td><td></td></tr><tr><td valign="top"><a href="#terminate-2">terminate/2</a></td><td></td></tr></table>


<a name="functions"></a>

## Function Details ##

<a name="assign_partitions-3"></a>

### assign_partitions/3 ###

<pre><code>
assign_partitions(Pid::pid(), Members::[<a href="brod.md#type-group_member">brod:group_member()</a>], TopicPartitions::[{<a href="brod.md#type-topic">brod:topic()</a>, <a href="brod.md#type-partition">brod:partition()</a>}]) -&gt; [{<a href="brod.md#type-group_member_id">brod:group_member_id()</a>, [<a href="brod.md#type-partition_assignment">brod:partition_assignment()</a>]}]
</code></pre>
<br />

<a name="assignments_received-4"></a>

### assignments_received/4 ###

<pre><code>
assignments_received(MemberPid::pid(), MemberId::<a href="brod.md#type-group_member_id">brod:group_member_id()</a>, GenId::<a href="brod.md#type-group_generation_id">brod:group_generation_id()</a>, Assignments::<a href="brod.md#type-received_assignments">brod:received_assignments()</a>) -&gt; ok
</code></pre>
<br />

<a name="assignments_revoked-1"></a>

### assignments_revoked/1 ###

<pre><code>
assignments_revoked(GroupMemberPid::pid()) -&gt; ok
</code></pre>
<br />

<a name="code_change-3"></a>

### code_change/3 ###

`code_change(OldVsn, State, Extra) -> any()`

<a name="get_committed_offsets-2"></a>

### get_committed_offsets/2 ###

<pre><code>
get_committed_offsets(GroupMemberPid::pid(), TopicPartitions::[{<a href="brod.md#type-topic">brod:topic()</a>, <a href="brod.md#type-partition">brod:partition()</a>}]) -&gt; {ok, [{{<a href="brod.md#type-topic">brod:topic()</a>, <a href="brod.md#type-partition">brod:partition()</a>}, <a href="brod.md#type-offset">brod:offset()</a>}]}
</code></pre>
<br />

<a name="handle_call-3"></a>

### handle_call/3 ###

`handle_call(Call, From, State) -> any()`

<a name="handle_cast-2"></a>

### handle_cast/2 ###

`handle_cast(Cast, St0) -> any()`

<a name="handle_info-2"></a>

### handle_info/2 ###

`handle_info(Info, St0) -> any()`

<a name="init-1"></a>

### init/1 ###

`init(Opts) -> any()`

<a name="start_link-1"></a>

### start_link/1 ###

<pre><code>
start_link(Opts::<a href="#type-opts">opts()</a>) -&gt; {ok, pid()} | {error, any()}
</code></pre>
<br />

<a name="terminate-2"></a>

### terminate/2 ###

`terminate(Reason, State) -> any()`

