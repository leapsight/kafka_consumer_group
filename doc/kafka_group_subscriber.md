

# Module kafka_group_subscriber #
* [Description](#description)
* [Data Types](#types)
* [Function Index](#index)
* [Function Details](#functions)

This module defines the kafka_group_subscriber.

__Behaviours:__ [`gen_server`](gen_server.md).

__This module defines the `kafka_group_subscriber` behaviour.__<br /> Required callback functions: `init/1`, `handle_message/5`.

<a name="description"></a>

## Description ##

## Callbacks
The user module should export:

```

  init(Args) ==> {ok, CallbackState}
```

```

  handle_message(Topic, Partition, Message, Subscriber, CallbackState)
   ==> {no_ack, NewCallbackState}
       | {ack, NewCallbackState}
       | {retry, NewCallbackState}
       | {async_retry, NewCallbackState}.
       | {error, overload, NewCallbackState}.
       | {error, Reason, NewCallbackState}.
```

Callback for a handler to process an individual kafka message.
Must return one of:

* `{no_ack, NewCallbackState}` - the callback handler takes responsibility of
asynchronously processing the message and acknowledging the offset later on
by calling [`kafka_group_subcriber:ack/2`](kafka_group_subcriber.md#ack-2). The handler is ready to
process the next message.
* `{ack, NewCallbackState}`  - the handler has completed processing
the message, the offset is committed and the handler is ready to process the
next message.
* `{retry, NewCallbackState}` - pauses the polling of messages from kafka
(temporarily unsubscribing from the brod_consumer, storing the message in a
local in-memory buffer and once the remaining messages in the set have been
processed, retries processing the enqueued messages N times each using an
exponential backoff strategy. Any message that could not be processed after
the max_retries has been reached is written in the provided Quarantine Topic
(QT) or discarded if one was not configured. If the application dies while
retrying, the buffer is lost.
* `{async_retry, NewCallbackState}`- we enqueue the message in a Retry
Queue (RQ) acknowledging it, while handling the retries
asynchronously and continuing processing the next message. The RQ is stored
in memory and/or in disk and can use a linear or exponential backoff
strategy. Also, in order to recover from a node failure the RQ can be
recreated at startup from a RQ Change Log that we generate by writing
operations into a Kafka Topic with compaction enabled. After a number
(max\_retries) of retries the message is dimmed unrecoverable and written to
a Quarantine Topic (also known as a Dead Letter Topic or Queue) if one was configured. The QT is defined in the topic specification of each topic provided
when starting a consumer group.
* `{error, overload, NewCallbackState}` - the handler cannot process more
messsages and wants to temporarily pause consuming messages.
The subscriber waits a period of time using an exponential backoff strategy
before calling the handler again with the last unprocessed message.
* `{error, Reason, NewCallbackState}` - if a QT
was configured it writes the message in the QT - the reason
is passed as an argument to the `quarantine_message_formatter`. Otherwise,
acknowledges the message (committing the offset) and continues processing
the next message.

While this callback function is being evaluated, the `brod` fetch-ahead
partition-consumers are fetching more messages behind the scene
unless `prefetch_count` is set to 0 in the `consumer_opts` config of the
consumer group (See [`kafka_consumer_group:start_group/1`](kafka_consumer_group.md#start_group-1)).

<a name="types"></a>

## Data Types ##




### <a name="type-opts">opts()</a> ###


<pre><code>
opts() = #{client_id =&gt; atom(), coordinator =&gt; pid(), generation_id =&gt; <a href="brod.md#type-group_generation_id">brod:group_generation_id()</a>, group_id =&gt; binary(), topic =&gt; <a href="kafka_consumer_group.md#type-topic_spec">kafka_consumer_group:topic_spec()</a>, partition =&gt; <a href="brod.md#type-partition">brod:partition()</a>, begin_offset =&gt; <a href="brod.md#type-offset">brod:offset()</a>, callback_mod =&gt; module(), callback_mod_args =&gt; any(), consumer_opts =&gt; #{}, producer_opts =&gt; #{}, group_opts =&gt; #{}}
</code></pre>

<a name="index"></a>

## Function Index ##


<table width="100%" border="1" cellspacing="0" cellpadding="2" summary="function index"><tr><td valign="top"><a href="#ack-2">ack/2</a></td><td></td></tr><tr><td valign="top"><a href="#code_change-3">code_change/3</a></td><td></td></tr><tr><td valign="top"><a href="#commit_offsets-1">commit_offsets/1</a></td><td>
Force commit collected (acked) offsets immediately.</td></tr><tr><td valign="top"><a href="#commit_offsets-2">commit_offsets/2</a></td><td>
Force commit collected (acked) offsets plus the given extra offsets
immediately.</td></tr><tr><td valign="top"><a href="#handle_call-3">handle_call/3</a></td><td></td></tr><tr><td valign="top"><a href="#handle_cast-2">handle_cast/2</a></td><td></td></tr><tr><td valign="top"><a href="#handle_info-2">handle_info/2</a></td><td></td></tr><tr><td valign="top"><a href="#init-1">init/1</a></td><td></td></tr><tr><td valign="top"><a href="#start_link-1">start_link/1</a></td><td>
ssss.</td></tr><tr><td valign="top"><a href="#terminate-2">terminate/2</a></td><td></td></tr></table>


<a name="functions"></a>

## Function Details ##

<a name="ack-2"></a>

### ack/2 ###

<pre><code>
ack(Subscriber::pid(), Offset::<a href="brod.md#type-offset">brod:offset()</a>) -&gt; ok
</code></pre>
<br />

<a name="code_change-3"></a>

### code_change/3 ###

`code_change(OldVsn, State, Extra) -> any()`

<a name="commit_offsets-1"></a>

### commit_offsets/1 ###

<pre><code>
commit_offsets(Subscriber::pid()) -&gt; ok
</code></pre>
<br />

Force commit collected (acked) offsets immediately.

<a name="commit_offsets-2"></a>

### commit_offsets/2 ###

<pre><code>
commit_offsets(Subscriber::pid(), Offsets::[<a href="brod.md#type-offset">brod:offset()</a>]) -&gt; ok
</code></pre>
<br />

Force commit collected (acked) offsets plus the given extra offsets
immediately.
NOTE: A lists:usrot is applied on the given extra offsets to commit
meaning if two or more offsets for the same topic-partition exist
in the list, only the one that is closer the head of the list is kept

<a name="handle_call-3"></a>

### handle_call/3 ###

`handle_call(Call, From, State) -> any()`

<a name="handle_cast-2"></a>

### handle_cast/2 ###

`handle_cast(Cast, State) -> any()`

<a name="handle_info-2"></a>

### handle_info/2 ###

`handle_info(Info, State0) -> any()`

<a name="init-1"></a>

### init/1 ###

`init(X1) -> any()`

<a name="start_link-1"></a>

### start_link/1 ###

`start_link(Opts) -> any()`

ssss

<a name="terminate-2"></a>

### terminate/2 ###

`terminate(Reason, State) -> any()`

