

# Module kafka_consumer_group_sup #
* [Description](#description)
* [Function Index](#index)
* [Function Details](#functions)

This module implements the  Kafka Consumer Group application top level
supervisor.

__Behaviours:__ [`supervisor`](supervisor.md).

<a name="description"></a>

## Description ##

The following diagram describes the supervision tree.

```

  +-----------------------------+
  |                             |
  |  kafka_consumer_group_sup   |
  |   one_for_one, transient    |
  |                             |
  +-----------------------------+
                 |
                /|\
  +-----------------------------+
  |                             |
  |       kafka_group_sup       |
  |   one_for_all, transient    |
  |                             |
  +-----------------------------+
                 |
                 +---------------------------------+
                 -                                 -
                 |                                 |
  +-----------------------------+   +-----------------------------+
  |                             |   |                             |
  |     kafka_group_member      |   | kafka_group_subscriber_sup  |
  |                             |   |simple_one_for_one, transient|
  |                             |   |                             |
  +-----------------------------+   +-----------------------------+
                 |                                 |
                 |                                /|\
                 |                  +-----------------------------+
                 |                  |                             |
                 |                  |   kafka_group_subscriber    |
                 |                  |                             |
                 |                  +-----------------------------+
                 |                                 |
                 -                                 -
                 |                                 |
  +-----------------------------+   +-----------------------------+
  |                             |   |                             |
  |      brod_coordinator       |   |        brod_consumer        |
  |                             |   |                             |
  +-----------------------------+   +-----------------------------+
                                                   |
                                                   -
                                                   |
                                    +-----------------------------+
                                    |                             |
                                    |       kafka partition       |
                                    |                             |
                                    +-----------------------------+
```
<a name="index"></a>

## Function Index ##


<table width="100%" border="1" cellspacing="0" cellpadding="2" summary="function index"><tr><td valign="top"><a href="#init-1">init/1</a></td><td></td></tr><tr><td valign="top"><a href="#start_group-1">start_group/1</a></td><td>
Starts a group consumer instance i.e.</td></tr><tr><td valign="top"><a href="#start_link-0">start_link/0</a></td><td></td></tr><tr><td valign="top"><a href="#stop_group-1">stop_group/1</a></td><td></td></tr></table>


<a name="functions"></a>

## Function Details ##

<a name="init-1"></a>

### init/1 ###

`init(X1) -> any()`

<a name="start_group-1"></a>

### start_group/1 ###

<pre><code>
start_group(Opts::<a href="kafka_group_member.md#type-opts">kafka_group_member:opts()</a>) -&gt; {ok, pid()} | {error, any}
</code></pre>
<br />

Starts a group consumer instance i.e. an instance of the kafka_group_sup
its child tree containing a subscriber per topic partition asigned to this
instance.

Opts is a map containing the following keys:
* client_id (required, atom) - brod client ID
* group_id (required, binary) -
* topics (required) - the list of topic specifications
* callback_mod (required, module) - the callback module that handles the messages
* callback_mod_args (required, map)- a map that will be passed to the callback Mod init function, if missing the empty map will be passed.
* group_opts (optional) - brod group opts,
* consumer_opts (optional) - brod consumer opts,
* producer_opts  (optional) - brod producer opts,

<a name="start_link-0"></a>

### start_link/0 ###

`start_link() -> any()`

<a name="stop_group-1"></a>

### stop_group/1 ###

`stop_group(GroupId) -> any()`

