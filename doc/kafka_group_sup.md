

# Module kafka_group_sup #
* [Function Index](#index)
* [Function Details](#functions)

__Behaviours:__ [`supervisor`](supervisor.md).

<a name="index"></a>

## Function Index ##


<table width="100%" border="1" cellspacing="0" cellpadding="2" summary="function index"><tr><td valign="top"><a href="#init-1">init/1</a></td><td></td></tr><tr><td valign="top"><a href="#start_link-1">start_link/1</a></td><td>
* ClientId - Client ID of the brod client.</td></tr><tr><td valign="top"><a href="#stop-1">stop/1</a></td><td>
Terminates the supervisor and all its children.</td></tr></table>


<a name="functions"></a>

## Function Details ##

<a name="init-1"></a>

### init/1 ###

`init(Opts) -> any()`

<a name="start_link-1"></a>

### start_link/1 ###

`start_link(Opts) -> any()`

* ClientId - Client ID of the brod client.
* GroupId: - Consumer group ID which should be unique per kafka cluster
* Topics - Predefined set of topic names to join the group.
* callback_mod - Callback module which should have the callback functions
implemented for message processing.
* callback_mod_args (required, map)

<a name="stop-1"></a>

### stop/1 ###

`stop(GroupId) -> any()`

Terminates the supervisor and all its children

