

# Module kafka_consumer_group #
* [Description](#description)
* [Data Types](#types)
* [Function Index](#index)
* [Function Details](#functions)

## Quarantine Topic
The purpose of the Quarantine Topic (QT) is to hold messages that could not
be processed.

<a name="description"></a>

## Description ##

Those messages can then be consumed from the QT for
inspection. An application might, with help of an operator, correct issues
and reprocess the message, log the fact that there was an error, and take
corrective action.

See how it works in [`kafka_group_subscriber:handle_message/5`](kafka_group_subscriber.md#handle_message-5) callback.
<a name="types"></a>

## Data Types ##




### <a name="type-opts">opts()</a> ###


<pre><code>
opts() = #{client_id =&gt; atom(), group_id =&gt; binary(), topics =&gt; [<a href="#type-topic_spec">topic_spec()</a>], callback_mod =&gt; module(), callback_mod_args =&gt; any(), consumer_opts =&gt; #{}, producer_opts =&gt; #{}, group_opts =&gt; #{}}
</code></pre>




### <a name="type-quarantine_message_formatter">quarantine_message_formatter()</a> ###


<pre><code>
quarantine_message_formatter() = fun((GroupId::binary(), <a href="brod.md#type-topic">brod:topic()</a>, <a href="brod.md#type-message">brod:message()</a>, Reason::any()) -&gt; {<a href="brod.md#type-partition">brod:partition()</a> | <a href="brod.md#type-partition_fun">brod:partition_fun()</a>, <a href="brod.md#type-key">brod:key()</a>, <a href="brod.md#type-value">brod:value()</a>})
</code></pre>




### <a name="type-topic_spec">topic_spec()</a> ###


<pre><code>
topic_spec() = #{name =&gt; <a href="brod.md#type-topic">brod:topic()</a>, max_retries =&gt; non_neg_integer(), quarantine_topic_name =&gt; <a href="brod.md#type-topic">brod:topic()</a>, quarantine_message_formatter =&gt; <a href="#type-quarantine_message_formatter">quarantine_message_formatter()</a>}
</code></pre>

<a name="index"></a>

## Function Index ##


<table width="100%" border="1" cellspacing="0" cellpadding="2" summary="function index"><tr><td valign="top"><a href="#start_group-1">start_group/1</a></td><td>
Starts a group consumer instance i.e.</td></tr><tr><td valign="top"><a href="#stop_group-1">stop_group/1</a></td><td></td></tr></table>


<a name="functions"></a>

## Function Details ##

<a name="start_group-1"></a>

### start_group/1 ###

<pre><code>
start_group(Opts::<a href="#type-opts">opts()</a>) -&gt; {ok, pid()} | {error, any}
</code></pre>
<br />

Starts a group consumer instance i.e. an instance of the kafka_group_sup
its child tree containing a subscriber per topic partition asigned to this
instance.

Opts is a map containing the following keys:
* client_id (required, atom) - brod client ID
* group_id (required, binary) -
* topics (required) - the list of topic specifications ( `topic_spec()` )
* callback_mod (required, module) - the callback module that handles the
messages
* callback_mod_args (required, map)- a map that will be passed to the
callback Mod init function, if missing the empty map will be passed.
* group_opts (optional) - brod group opts,
* consumer_opts (optional) - brod consumer opts,
* producer_opts  (optional) - brod producer opts

<a name="stop_group-1"></a>

### stop_group/1 ###

`stop_group(GroupId) -> any()`

