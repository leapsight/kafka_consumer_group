

# kafka_consumer_group #

Copyright (c) 2017 Ngineo Limited t/a Leapsight

__Version:__ 0.1

## Kafka Consumer Group (KCG)
An erlang app which is a brod (kafka client) wrapper implementing a consumer group where every topic-partition is handled by its own process.

Specifically, KCG has The following characteristics:

* A single function to start a group (see [`kafka_consumer_group:start_group/1`](https://gitlab.com/leapsight/kafka_consumer_group/blob/develop/doc/kafka_consumer_group.md#start_group-1)), no additional configuration is required.
* Every group has N associated topics
* Every group has its own supervision tree (see [`kafka_consumer_group_sup`](https://gitlab.com/leapsight/kafka_consumer_group/blob/develop/doc/kafka_consumer_group_sup.md)) under the root supervisor.
* One erlang process (see [`kafka_group_supervisor`](https://gitlab.com/leapsight/kafka_consumer_group/blob/develop/doc/kafka_group_supervisor.md)) per topic partition in the group
* The application crashes every time brod cannot get a connection with Kafka after a number of retries.
* Support for Quarantine topics (Dead-letter topic) per topic
* Overload support, pausing consumption using an exponential backoff strategy


## Modules ##


<table width="100%" border="0" summary="list of modules">
<tr><td><a href="https://gitlab.com/leapsight/kafka_consumer_group/blob/develop/doc/kafka_consumer_group.md" class="module">kafka_consumer_group</a></td></tr>
<tr><td><a href="https://gitlab.com/leapsight/kafka_consumer_group/blob/develop/doc/kafka_consumer_group_app.md" class="module">kafka_consumer_group_app</a></td></tr>
<tr><td><a href="https://gitlab.com/leapsight/kafka_consumer_group/blob/develop/doc/kafka_consumer_group_sup.md" class="module">kafka_consumer_group_sup</a></td></tr>
<tr><td><a href="https://gitlab.com/leapsight/kafka_consumer_group/blob/develop/doc/kafka_group_member.md" class="module">kafka_group_member</a></td></tr>
<tr><td><a href="https://gitlab.com/leapsight/kafka_consumer_group/blob/develop/doc/kafka_group_monitor.md" class="module">kafka_group_monitor</a></td></tr>
<tr><td><a href="https://gitlab.com/leapsight/kafka_consumer_group/blob/develop/doc/kafka_group_subscriber.md" class="module">kafka_group_subscriber</a></td></tr>
<tr><td><a href="https://gitlab.com/leapsight/kafka_consumer_group/blob/develop/doc/kafka_group_subscriber_sup.md" class="module">kafka_group_subscriber_sup</a></td></tr>
<tr><td><a href="https://gitlab.com/leapsight/kafka_consumer_group/blob/develop/doc/kafka_group_sup.md" class="module">kafka_group_sup</a></td></tr>
<tr><td><a href="https://gitlab.com/leapsight/kafka_consumer_group/blob/develop/doc/kafka_group_utils.md" class="module">kafka_group_utils</a></td></tr></table>

