# Quarantine Topic aka Dead Letter Topic (DLT)
https://app.craft.io/share/974A387C2305843009784619471

> Note that unlike traditional pub/sub messaging systems, you commit offsets and not ack individual messages.
> This means that if you failed to process record #30 and succeeded in processing record #31, you should not commit record #31 — this would result in committing all the records up to #31 including #30, which is usually not what you want.
>-- p128 of Kafka Book

# Solution 
Implement it as a feature in Leapsight's kafa_consumer_group lib so that we can reuse across all other µservices.


When `kafka_group_subscriber:handle_message` encounters a retriable error while processing a message it has a number of options:

1. Request a sync_retry - we can pause polling messages (temporarily unsubscribing from the brod_consumer, store the message in a local buffer and once kafka_group_subscriber:handle_messages() has finished, retry processing the enqueued messages N times each. If the node dies, the buffer is lost.
1. Request an async_retry - we enqueue the message in the Retry Queue (RQ) and continue acknowledging it, while handling the retries asynchronously. The RQ is stored in memory and can use a linear or exponential backoff strategy. Also, in order to recover from a node failure the RQ can be recreated at startup from a RQ Change Log that we generate by writing operations into a Kafka Topic with compaction enabled. After a number (max\_retries) of retries the message is dimmed unrecoverable and written to a Quarantine Topic (also known as a Dead Letter Topic or Queue). The QT is defined in the topic specification of each topic provided when starting a consumer group.
1. Continue without acknowledging the message - the callback handler takes responsibility of asynchronously processing the message and acknowledging the offset later on.
1. Continue acknowledging the message - the callback handler takes responsibility of asynchronously processing the message.
1. In case of overload, pause the polling of messages using an exponential backoff strategy

#Retry Queue
A queue of tasks (high order functions aka funs in erlang) using the jobs library and backed up by a Change Log in a kafka topic. We could also implement a custom jobs queue which stores teh queu in local disk storage.

# Quarantine Topic
The purpose of the Quarantine Topic (QT) is to hold messages that could not be processed. Messages can then be consumed from the QT for inspection. An application might, with help of an operator, correct issues and reprocess the message, log the fact that there was an error, and take corrective action.

We can:
* configure one QT per topic where we write the unprocessed message
* configure one global QT per Group ID where message is a wrapper containing metadata such as Topic, Offset and the unprocessed message
* configure one global QT where message is a wrapper containing metadata such as GroupID, Topic, Offset and the unprocessed message


### Inspiration:
* Work we did on XAPO on Retries Queues using Ulf Wiger's jobs library
* Work we did on William Hill on Retries Queues with TTL using Leapsight's tuplespace library
* [MS](https://docs.microsoft.com/en-us/azure/service-bus-messaging/service-bus-dead-letter-queues)
* [AWS SQS DLQ](http://docs.aws.amazon.com/AWSSimpleQueueService/latest/SQSDeveloperGuide/sqs-dead-letter-queues.html)
* [Kafka Streams Error handling](https://docs.confluent.io/current/streams/faq.html#handling-corrupted-records-and-deserialization-errors-poison-pill-messages)
* https://issues.apache.org/jira/browse/KAFKA-3895