%%%-------------------------------------------------------------------
%% @doc group public API
%% @end
%%%-------------------------------------------------------------------

-module(group_app).
-behaviour(application).
-include_lib("brod/include/brod.hrl").

%% Application callbacks
-export([start/2, stop/1, init/1, handle_message/5]).

%%====================================================================
%% API
%%====================================================================

start(_StartType, _StartArgs) ->

    Res = group_sup:start_link(),
    kafka_consumer_group:start_group(#{
        client_id => kafka_client_1,
        topics => [
            #{
                name => <<"test_topic">>,
                max_retries => 3,
                quarantine_topic_name => <<"test_quarantine_topic">>,
                quarantine_message_formatter => fun
                        (GroupId, Topic, M, Reason) ->
                            #kafka_message{
                                key = Key,
                                value = Value,
                                offset = Offset
                            } = M,
                            {
                                fun(_Topic, PartCount, _Key, _Value) ->
                                    {ok, hash:fnv32a(GroupId) rem PartCount}
                                end,
                                Key,
                                msgpack:pack(#{
                                    offset => Offset,
                                    message => Value,
                                    group_id => GroupId,
                                    topic => Topic,
                                    reason => Reason
                                })
                            }
                    end
            }
        ],
        group_id => <<"test_consumer_group">>,
        callback_mod => ?MODULE,
        callback_mod_args => #{pid => self()},
        consumer_opts => #{
            begin_offset => earliest
        }
    }),

    Res.

%%--------------------------------------------------------------------
stop(_State) ->
    ok.



init(Map) ->
    {ok, Map}.


handle_message(Topic, Part, M, _Subs, #{pid := Pid} = St) ->
    #kafka_message{
        offset = Offset,
        magic_byte = _,
        attributes = _,
        key = Key,
        value = Value,
        crc = _,
        ts_type = _,
        ts = _
    } = M,

    L = [Topic, Part, Offset, Key, msgpack:unpack(Value)],
    Pid ! {test_message, L},
    {error, test, St}.


%%====================================================================
%% Internal functions
%%====================================================================


