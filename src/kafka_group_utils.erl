-module(kafka_group_utils).


-define(GROUP_OPTS, #{
    %% This is the protocol name used when join a group, if not given,
    %% by default `partition_assignment_strategy' is used as the protocol name.
    %% Setting a protocol name allows to interact with consumer group members
    %% designed in other programing languages. For example, 'range' is the most
    %% commonly used protocol name for JAVA client. However, brod only supports
    %% roundrobin protocol out of the box, in order to mimic 'range' protocol
    %% one will have to do it via `callback_implemented' assignment strategy
    protocol_name => #{
        required => true,
        datatype => {in, [roundrobin_v2]},
        default => roundrobin_v2
    },
    %% roundrobin (topic-sticky):
    %%   Take all topic-offset (sorted [{TopicName, Partition}] list)
    %%   assign one to each member in a roundrobin fashion. However only
    %%   partitions in the subscription topic list are assigned.
    %% callback_implemented
    %%   Call CbModule:assign_partitions/2 to assign partitions.
    partition_assignment_strategy => #{
        required => true,
        datatype => {in, [roundrobin_v2, callback_implemented]},
        default => roundrobin_v2
    },
    %% Time in seconds for the group coordinator broker to consider a member
    %% 'down' if no heartbeat or any kind of requests received from a broker
    %% in the past N seconds.
    %% A group member may also consider the coordinator broker 'down' if no
    %% heartbeat response response received in the past N seconds.
    %% Kafka docs:
    %% The timeout used to detect consumer failures when using Kafka's group
    %% management facility. The consumer sends periodic heartbeats to indicate
    %% its liveness to the broker. If no heartbeats are received by the broker
    %% before the expiration of this session timeout, then the broker will
    %% remove this consumer from the group and initiate a rebalance. Note that
    %% the value must be in the allowable range as configured in the broker
    %% configuration by group.min.session.timeout.ms and
    %% group.max.session.timeout.ms.
    session_timeout_seconds => #{
        required => true,
        datatype => non_neg_integer,
        default => 60
    },
    %% Time in seconds for the member to 'ping' the group coordinator.
    %% Kafka docs:
    %% The expected time in seconds between heartbeats to the consumer
    %% coordinator when using Kafka's group management facilities. Heartbeats
    %% are used to ensure that the consumer's session stays active and to
    %% facilitate rebalancing when new consumers join or leave the group. The
    %% value must be set lower than session.timeout.ms,
    %% but typically should be set no higher than 1/3 of that value. It can be
    %% adjusted even lower to control the expected time for normal rebalances.
    %% OBS: Care should be taken when picking the number, on one hand, we do
    %%      not want to flush the broker with requests if we set it too low,
    %%      on the other hand, if set it too high, it may take too long for
    %%      the members to realise status changes of the group such as
    %%      assignment rebalacing or group coordinator switchover etc.
    heartbeat_rate_seconds => #{
        required => true,
        datatype => non_neg_integer,
        default => 2
    },
    %% Maximum number of times allowd for a member to re-join the group.
    %% The gen_server will stop if it reached the maximum number of retries.
    %% OBS: 'let it crash' may not be the optimal strategy here because
    %%      the group member id is kept in the gen_server looping state and
    %%      it is reused when re-joining the group.
    max_rejoin_attempts => #{
        required => true,
        datatype => non_neg_integer,
        default => 5
    },
    %% Delay in seconds before re-joining the group.
    rejoin_delay_seconds => #{
        required => true,
        datatype => non_neg_integer,
        default => 5
    },
    %% How/where to commit offsets, possible values:
    %%   - commit_to_kafka_v2:
    %%      Group coordinator will commit the offsets to kafka using
    %%      version 2 OffsetCommitRequest.
    %%   - consumer_managed:
    %%      The group member (e.g. brod_group_subscriber.erl) is responsible
    %%      for persisting offsets to a local or centralized storage.
    %%      And the callback get_committed_offsets should be implemented
    %%      to allow group coordinator to retrieve the commited offsets.
    offset_commit_policy => #{
        required => true,
        datatype => {in, [commit_to_kafka_v2, consumer_managed]},
        default => commit_to_kafka_v2
    },
    %% The time interval between two OffsetCommitRequest messages.
    %% This config is irrelevant if offset_commit_policy is consumer_managed.
    offset_commit_interval_seconds => #{
        required => true,
        datatype => non_neg_integer,
        default => 5
    },
    %% How long the time is to be kept in kafka before it is deleted.
    %% The default special value -1 indicates that the __consumer_offsets
    %% topic retention policy is used.
    %% This config is irrelevant if offset_commit_policy is consumer_managed.
    offset_retention_seconds => #{
        required => false,
        datatype => integer,
        default => undefined,
        validator => fun
            (X) when X >= -1 orelse X =:= undefined -> true;
            (_) -> false
        end
    }
}).


-define(PRODUCER_OPTS, #{
    %% How many acknowledgements the kafka broker should receive from the
    %% clustered replicas before acking producer.
    %%    0: the broker will not send any response
    %%      (this is the only case where the broker will not reply to a request)
    %%    1: The leader will wait the data is written to the local log before
    %%   sending a response.
    %%   -1: If it is -1 the broker will block until the message is committed by
    %%       all in sync replicas before acking.
    required_acks => #{
        required => true,
        datatype => {in, [0, 1, -1]},
        default => -1
    },
    %% Maximum time in milliseconds the broker can await the receipt of the
    %% number of acknowledgements in RequiredAcks. The timeout is not an exact
    %% limit on the request time for a few reasons: (1) it does not include
    %% network latency, (2) the timer begins at the beginning of the processing
    %% of this request so if many requests are queued due to broker overload
    %% that wait time will not be included, (3) kafka leader will not terminate
    %% a local write so if the local write time exceeds this timeout it will
    %% not be respected.
    ack_timeout => #{
        required => true,
        datatype => non_neg_integer,
        default => 10000
    },
    %% How many requests (per-partition) can be buffered without blocking the
    %% caller. The callers are released (by receiving the
    %% 'brod_produce_req_buffered' reply) once the request is taken into buffer
    %% and after the request has been put on wire, then the caller may expect
    %% a reply 'brod_produce_req_acked' when the request is accepted by kafka.
    partition_buffer_limit => #{
        required => true,
        datatype => non_neg_integer,
        default => 256
    },
    %% How many message sets (per-partition) can be sent to kafka broker
    %% asynchronously before receiving ACKs from broker.
    %% NOTE: setting a number greater than 1 may cause messages being persisted
    %% in an order different from the order they were produced.
    partition_onwire_limit => #{
        required => true,
        datatype => non_neg_integer,
        default => 1
    },
    %% In case callers are producing faster than brokers can handle (or
    %% congestion on wire), try to accumulate small requests into batches
    %% as much as possible but not exceeding max_batch_size.
    %% OBS: If compression is enabled, care should be taken when picking
    %% the max batch size, because a compressed batch will be produced
    %% as one message and this message might be larger than
    %% 'max.message.bytes' in kafka config (or topic config)
    max_batch_size => #{
        required => true,
        datatype => non_neg_integer,
        default => 1048576
    },
    %% If {max_retries, N} is given, the producer retry produce request for
    %% N times before crashing in case of failures like socket being shut down
    %% or exceptions received in produce response from kafka.
    %% The special value N = -1 means 'retry indefinitely'
    max_retries => #{
        required => true,
        datatype => integer,
        default => 3,
        validator => fun
            (X) when X >= -1 -> true;
            (_) -> false
        end
    },
    %% Time in milli-seconds to sleep before retry the failed produce request.
    retry_backoff_ms => #{
        required => true,
        datatype => non_neg_integer,
        default => 500
    },
    compression => #{
        required => true,
        datatype => {in, [no_compression, gzip, snappy]},
        default => no_compression
    },
    %% Only try to compress when batch size is greater than this value, in KBs
    min_compression_batch_size => #{
        required => true,
        datatype => pos_integer,
        default => 1024
    },
    %% Messages are allowed to 'linger' in buffer for this amount of
    %% milli-seconds before being sent.
    %% Definition of 'linger': A message is in 'linger' state when it is allowed
    %% to be sent on-wire, but chosen not to (for better batching).
    %% The default value is 0 for 2 reasons:
    %% 1. Backward compatibility (for 2.x releases)
    %% 2. Not to surprise `brod:produce_sync' callers
    max_linger_ms => #{
        required => true,
        datatype => non_neg_integer,
        default => 0
    },
    %% At most this amount (count not size) of messages are allowed to 'linger'
    %% in buffer. Messages will be sent regardless of 'linger' age when this
    %% threshold is hit.
    %% NOTE: It does not make sense to have this value set larger than
    %% `partition_buffer_limit'
    max_linger_count => #{
        required => true,
        datatype => non_neg_integer,
        default => 0
    }
}).


-define(SSL_OPTS, #{
    certfile => #{
        required => true,
        datatype => [binary, list]
    },
    keyfile => #{
        required => true,
        datatype => [binary, list]
    },
    cacertfile => #{
        required => true,
        datatype => [binary, list]
    }
}).

-define(SASL_OPTS, #{
    username => #{
        required => true,
        datatype => [binary, list]
    },
    password => #{
        required => true,
        datatype => [binary, list]
    }
}).

-define(CONSUMER_OPTS, #{
    %% Minimal bytes to fetch in a batch of messages
    min_bytes => #{
        required => true,
        datatype => non_neg_integer,
        default => 0
    },
    %% Maximum bytes to fetch in a batch of messages (1MB)
    %% NOTE: this value might be expanded to retry when it is not enough
    %%  to fetch even one single message, then slowly shrinked back
    %%  to this given value.
    max_bytes => #{
        required => true,
        datatype => non_neg_integer,
        default => 1048576
    },
    %% Max number of seconds allowd for the broker to collect min_bytes of
    %% messages in fetch response
    max_wait_time => #{
        required => true,
        datatype => non_neg_integer,
        default => 10000
    },
    %% Allow consumer process to sleep this amout of ms if kafka replied
    %% 'empty' message-set.
    sleep_timeout => #{
        required => true,
        datatype => non_neg_integer,
        default => 10000
    },
    %% The window size (number of messages) allowed to fetch-ahead.
    prefetch_count => #{
        required => true,
        datatype => pos_integer,
        default => 1
    },
    %% The offset from which to begin fetch requests.
    begin_offset => #{
        required => true,
        datatype => [integer, {in, [earliest, latest]}],
        default => earliest
    },
    %% How to reset begin_offset if OffsetOutOfRange exception is received.
    %% reset_by_subscriber: consumer is suspended (is_suspended=true in state)
    %% and wait for subscriber to re-subscribe with a new
    %% 'begin_offset' option.
    %% reset_to_earliest: consume from the earliest offset.
    %% reset_to_latest: consume from the last available offset.
    offset_reset_policy => #{
        required => true,
        datatype => {in, [
            reset_by_subscriber, reset_to_earliest, reset_to_latest
        ]},
        default => reset_to_latest
    }
}).

-define(CLIENT_OPTS, #{
    %% How much time to wait between attempts to restart brod_client
    %% process when it crashes
    restart_delay_seconds => #{
        required => true,
        datatype => non_neg_integer,
        default => 10
    },
    %% Number of retries if failed fetching metadata due to socket error
    max_metadata_sock_retry => #{
        required => true,
        datatype => non_neg_integer,
        default => 1
    },
    %% Return timeout error from brod_client:get_metadata/2 in case the
    %% response is not received from kafka in this configured time.
    get_metadata_timeout_seconds => #{
        required => true,
        datatype => non_neg_integer,
        default => 5
    },
    %% Delay this configured number of seconds before retrying to
    %% estabilish a new connection to the kafka partition leader.
    reconnect_cool_down_seconds => #{
        required => true,
        datatype => non_neg_integer,
        default => 1
    },
    %% By default, brod respects what is configured in broker about
    %% topic auto-creation. i.e. whatever auto.create.topics.enable
    %% is set in borker configuration.
    %% However if 'allow_topic_auto_creation' is set to 'false' in client
    %% config, brod will avoid sending metadata requests that may cause an
    %% auto-creation of the topic regardless of what the broker config is.
    allow_topic_auto_creation => #{
        required => true,
        datatype => boolean,
        default => false
    },
    %% If true, brod client will spawn a producer automatically when
    %% user is trying to call 'produce' but did not call
    %% brod:start_producer explicitly. Can be useful for applications
    %% which don't know beforehand which topics they will be working with.
    auto_start_producers => #{
        required => true,
        datatype => boolean,
        default => false
    },
    %% Producer configuration to use when auto_start_producers is true.
    %% @see brod_producer:start_link/4. for details about producer config
    default_producer_config => #{
        required => true,
        datatype => map,
        default => #{},
        validator => ?PRODUCER_OPTS
    },
    %% When true, brod will try to upgrade tcp connection to ssl using default
    %% ssl options. List of ssl options implies ssl=true.
    ssl => #{
        required => true,
        datatype => [boolean, map],
        default => false,
        validator => fun
            (X) when is_map(X) ->
                {ok, maps_utils:validate(X, ?SSL_OPTS)};
            (X) when is_boolean(X) ->
                {ok, X};
            (_) ->
                false
        end
    },
    %% Credentials for SASL/Plain authentication.
    sasl => #{
        required => true,
        default => undefined,
        validator => fun
            (X) when is_map(X) ->
                {ok, maps_utils:validate(X, ?SASL_OPTS)};
            (undefined) ->
                {ok, undefined};
            (_) ->
                false
        end
    },
    %% Timeout when trying to connect to one endpoint.
    connect_timeout => #{
        required => true,
        datatype => non_neg_integer,
        default => 5000
    },
    %% Timeout when waiting for a response, socket restart when timedout.
    request_timeout  => #{
        required => true,
        datatype => non_neg_integer,
        default => 240000,
        validator => fun
            (X) when X >= 1000 -> true;
            (_) -> false
        end
    }
}).



-export([validate_client_opts/1]).
-export([validate_consumer_opts/1]).
-export([validate_group_opts/1]).
-export([validate_producer_opts/1]).
-export([get_client_config/1]).
-export([get_all_client_configs/0]).



%% =============================================================================
%% API
%% =============================================================================




%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec validate_client_opts(map()) -> [{atom(), any()}] | no_return().

validate_client_opts(Opts) ->
    maps:to_list(maps_utils:validate(Opts, ?CLIENT_OPTS)).


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec validate_consumer_opts(map()) -> [{atom(), any()}] | no_return().

validate_consumer_opts(Opts) ->
    maps:to_list(maps_utils:validate(Opts, ?CONSUMER_OPTS)).


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec validate_producer_opts(map()) -> [{atom(), any()}] | no_return().

validate_producer_opts(Opts) ->
    maps:to_list(maps_utils:validate(Opts, ?PRODUCER_OPTS)).


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec validate_group_opts(map()) -> [{atom(), any()}] | no_return().

validate_group_opts(Opts) ->
    maps:to_list(maps_utils:validate(Opts, ?GROUP_OPTS)).




%% -----------------------------------------------------------------------------
%% @doc
%% Returns {Endpoints, Name, Config}
%% @end
%% -----------------------------------------------------------------------------
get_client_config(ClientId) ->
    case application:get_env(kafka_consumer_group, kafka) of
        {ok, PL} ->
            L = [
                T ||
                {_, Id, _} = T <- parse_kafka_options(PL),
                Id =:= ClientId
            ],
            hd(L);
        undefined ->
            erlang:exit(invalid_kafka_config)
    end.


get_all_client_configs() ->
    case application:get_env(kafka_consumer_group, kafka) of
        {ok, PL} ->
            [T || T <- parse_kafka_options(PL)];
        undefined ->
            erlang:exit(invalid_kafka_config)
    end.


%% -----------------------------------------------------------------------------
%% @doc
%% {kafka, [
%%     {clients, [
%%         {kafka_client_1, [
%%             {endpoints, [
%%                 {"localhost", 9092}
%%             ]},
%%             {reconnect_cool_down_seconds, 10},
%%             {auto_start_producers, true},
%%             {default_producer_config, [
%%                 {topic_restart_delay_seconds, 10},
%%                 {partition_restart_delay_seconds, 2},
%%                 {required_acks, -1}
%%             ]}
%%         ]}
%%     ]}
%% ]},
%% ]}
%% @end
%% -----------------------------------------------------------------------------
parse_kafka_options(PL) ->
    case lists:keyfind(clients, 1, PL) of
        {clients, CPL} ->
            parse_kafka_options(CPL, []);
        false ->
            erlang:exit(invalid_kafka_config)
    end.


parse_kafka_options([{Name, Opts}|T], Acc) ->
    case lists:keytake(endpoints, 1, Opts) of
        {value, {endpoints, Endpoints}, Config} ->
            parse_kafka_options(T, [{Endpoints, Name, Config}|Acc]);
        false ->
            erlang:exit(invalid_kafka_config)
    end;

parse_kafka_options([], Acc) ->
    Acc.

