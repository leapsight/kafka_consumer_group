%% -----------------------------------------------------------------------------
%% @doc
%% This server monitors the kafka_group_sup to detect when they reached the
%% reached_max_restart_intensity and kills the application.
%% @end
%% -----------------------------------------------------------------------------
-module(kafka_group_monitor).
-behaviour(gen_server).

-type index() :: #{
    reference() | pid() => pid() | reference()
}.

-type group() :: #{
    restarts => pos_integer(),
    max_restarts => pos_integer()
}.

-record(state, {
    groups = #{}        ::  #{binary() => group()},
    processes = #{}     ::  index()
}).

%% API
-export([start_link/0]).
-export([monitor_me/1]).
-export([demonitor_me/1]).


%% GEN_SERVER CALLBACKS
-export([code_change/3]).
-export([handle_call/3]).
-export([handle_cast/2]).
-export([handle_info/2]).
-export([init/1]).
-export([terminate/2]).
-export([add_group/2]).
-export([remove_group/1]).



%% =============================================================================
%% API
%% =============================================================================


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec start_link() -> {ok, pid()} | {error, any()}.

start_link() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec add_group(binary(), pos_integer()) -> ok.

add_group(GroupId, MaxRestarts) ->
    gen_server:call(?MODULE, {add_group, GroupId, MaxRestarts}).


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec remove_group(binary()) -> ok.

remove_group(GroupId) ->
    gen_server:call(?MODULE, {remove_group, GroupId}).


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec monitor_me(pid()) -> ok.

monitor_me(Pid) ->
    gen_server:call(?MODULE, {monitor_me, Pid}).


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec demonitor_me(pid()) -> ok.

demonitor_me(Pid) ->
    gen_server:call(?MODULE, {demonitor_me, Pid}).




%% =============================================================================
%% GEN_SERVER CALLBACKS
%% =============================================================================

init([])  ->
    erlang:process_flag(trap_exit, true),
    {ok, #state{}}.

handle_call({add_group, GroupId, MaxRestarts}, _From, State) ->
    Map0 = State#state.groups,
    Group = #{max_restarts => MaxRestarts, restarts => 0},
    Map1 = maps:put(GroupId, Group, Map0),
    {reply, ok, State#state{groups = Map1}};

handle_call({remove_group, GroupId}, _From, State) ->
    Map0 = State#state.groups,
    {reply, ok, State#state{groups = maps:remove(GroupId, Map0)}};


handle_call({monitor_me, Pid}, _From, State) ->
    Map0 = State#state.processes,
    Ref = erlang:monitor(process, Pid),
    Map1 = maps:put(Ref, Pid, Map0),
    Map2 = maps:put(Pid, Ref, Map1),
    {reply, ok, State#state{processes = Map2}};

handle_call({demonitor_me, Pid}, _From, State) ->
    Map0 = State#state.processes,
    case maps:get(Pid, Map0, undefined) of
        undefined ->
            {reply, {error, {unknown_process, Pid}}, State};
        Ref ->
            Map1 = maps:remove(Pid, Map0),
            Map2 = maps:remove(Ref, Map1),
            true = erlang:demonitor(Ref),
            {reply, ok, State#state{processes = Map2}}
    end.


handle_cast(Cast, State) ->
    _ = lager:info("Unknown cast message; message=~p", [Cast]),
    {noreply, State}.


handle_info({'DOWN', Ref, process, Object, Info}, State) ->
    case maps:get(Ref, State#state.processes, undefined) of
        undefined ->
            _ = lager:info(
                "Received unknown DOWN message; process=~p, info=~p",
                [Object, Info]
            ),
            {noreply, State};
        _Pid ->
            _ = lager:info(
                "Received DOWN message; process=~p, info=~p",
                [Object, Info]
            ),
            true = erlang:demonitor(Ref),
            Map = maps:remove(Ref, State#state.processes),
            %% application:stop(kafka_consumer_group),
            true = exit(whereis(kafka_consumer_group_sup), kill),
            {noreply, State#state{processes = Map}}
    end;

handle_info(Info, State) ->
    _ = lager:error(
        "Received unknown info message; message=~p, state=~p", [Info, State]),
    {stop, unknown_message, State}.


code_change(_OldVsn, State, _Extra) ->
    {ok, State}.


terminate(Reason, State) ->
    _ = [
        erlang:demonitor(Ref) ||
        Ref <- maps:keys(State#state.processes), is_reference(Ref)
    ],
    _ = lager:info("Terminated; reason=~p", [Reason]),
    ok.





%% =============================================================================
%% PRIVATE
%% =============================================================================


