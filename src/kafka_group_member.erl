%% -----------------------------------------------------------------------------
%% @doc
%% This module represents a Kafka Consumer Group member. Typically we will have
%% one of these per erlang node per GroupId.
%% It implements brod_group_member and sets up one subscriber (worker) per
%% topic per partition (as it allows to setup a group with multiple topics)
%% @end
%% -----------------------------------------------------------------------------
-module(kafka_group_member).
-behaviour(gen_server).
-behaviour(brod_group_member).

-include_lib("brod/include/brod.hrl").

-define(BACKOFF_MAX, 30000).
-define(MAX_RETRIES, 5).

-record(subscriber, {
    partition       ::  brod:partition(),
    begin_offset    ::  brod:offset(),
    topic           ::  brod:topic(),
    pid             ::  pid(),
    missing_since   ::  erlang:timestamp()
}).

-record(state, {
    client_id               ::  atom(),
    endpoints               ::  list(),
    config                  ::  [{atom(), term()}],
    coordinator             ::  pid() | undefined,
    group_id                ::  binary(),
    member_id               ::  brod:member_id() | undefined,
    generation_id           ::  brod:generation_id() | undefined,
    topics = #{}            ::  #{brod:topic() =>
                                    kafka_consumer_group:topic_spec()},
    callback_mod            ::  module(),
    callback_mod_args       ::  any(),
    group_opts = #{}        ::  map(),
    consumer_opts = #{}     ::  map(),
    producer_opts = #{}     ::  map(),
    subscriber_id           ::  pid() | undefined,
    subscribers = []        ::  [#subscriber{}],
    backoff_max = ?BACKOFF_MAX     ::  pos_integer() | infinity,
    backoff                 ::  any() | undefined,
    backoff_timer           ::  reference(),
    max_retries = ?MAX_RETRIES :: pos_integer(),
    retries = 0             :: pos_integer()
}).


-type state()           ::  #state{}.

-type opts()        ::  #{
    client_id => atom(),
    group_id => binary(),
    topics => [kafka_consumer_group:topic_spec()],
    callback_mod => module(),
    callback_mod_args => any(),
    consumer_opts => map(),
    producer_opts => map(),
    group_opts => map()
}.

-export_type([opts/0]).

%% API
-export([start_link/1]).

%% GEN_SERVER CALLBACKS
-export([code_change/3]).
-export([handle_call/3]).
-export([handle_cast/2]).
-export([handle_info/2]).
-export([init/1]).
-export([terminate/2]).

%% BROD_GROUP_MEMBER CALLBACKS
-export([assign_partitions/3]).
-export([assignments_received/4]).
-export([assignments_revoked/1]).
-export([get_committed_offsets/2]).



%% =============================================================================
%% API
%% =============================================================================



%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec start_link(opts()) -> {ok, pid()} | {error, any()}.

start_link(Opts) ->
    gen_server:start_link(?MODULE, Opts, []).



%% =============================================================================
%% GROUP_MEMBER CALLBACKS
%% =============================================================================



%% Called when assignments are received from group leader.
%% the member process should now call brod:subscribe/5
%% to start receiving message from kafka.
-spec assignments_received(
    pid(),
    brod:group_member_id(),
    brod:group_generation_id(),
    brod:received_assignments()) -> ok.

assignments_received(MemberPid, MemberId, GenId, Assignments) ->
    _ = lager:info(
        "Assignments received; member_pid=~p, member_id=~p",
        [MemberPid,MemberId]
    ),
    Msg = {assignments_received, MemberId, GenId, Assignments},
    gen_server:cast(MemberPid, Msg).


%% Called before group re-balancing, the member should call
%% brod:unsubscribe/3 to unsubscribe from all currently subscribed partitions.
-spec assignments_revoked(pid()) -> ok.

assignments_revoked(GroupMemberPid) ->
    _ = lager:info("Assignments revoked; pid=~p", [GroupMemberPid]),
    gen_server:cast(GroupMemberPid, assignments_revoked).


-spec get_committed_offsets(pid(), [{brod:topic(), brod:partition()}]) ->
    {ok, [{{brod:topic(), brod:partition()}, brod:offset()}]}.

get_committed_offsets(_GroupMemberPid, _TopicPartitions) ->
    erlang:exit({not_yet_implemented, get_committed_offsets}).


-spec assign_partitions(
    pid(), [brod:group_member()], [{brod:topic(), brod:partition()}]) ->
  [{brod:group_member_id(), [brod:partition_assignment()]}].

assign_partitions(_Pid, _Members, _TopicPartitions) ->
    _ = lager:warning("Assign partitions called, function not implemented."),
    erlang:exit({not_yet_implemented, assign_partitions}).



%% =============================================================================
%% GEN_SERVER CALLBACKS
%% =============================================================================



init(Opts)  ->
    erlang:process_flag(trap_exit, true),
    #{
        client_id := ClientId,
        group_id := GroupId,
        topics := Topics
    } = Opts,
    ok = brod_utils:assert_client(ClientId),
    ok = brod_utils:assert_group_id(GroupId),
    ok = brod_utils:assert_topics([maps:get(name, T) || T <- Topics]),
    self() ! {finish_init, Opts},
    {ok, #state{}}.


handle_info({finish_init, Opts}, St0) ->
    #{
        client_id := ClientId,
        group_id := GroupId,
        topics := Topics,
        callback_mod := Mod,
        callback_mod_args := ModArgs
    } = Opts,

    GroupOpts = kafka_group_utils:validate_group_opts(
        maps:get(group_opts, Opts, #{})
    ),
    ConsumerOpts = kafka_group_utils:validate_consumer_opts(
        maps:get(consumer_opts, Opts, #{})
    ),
    ProducerOpts = kafka_group_utils:validate_producer_opts(
        maps:get(producer_opts, Opts, #{})
    ),
    TopicMap = maps:from_list([{maps:get(name, T), T} || T <- Topics]),

    {Endpoints, _, Config} = kafka_group_utils:get_client_config(ClientId),

    Max = ?BACKOFF_MAX,
    B = backoff:type(backoff:init(1000, Max), jitter),

    St1 = St0#state{
        backoff = B,
        backoff_max = Max,
        client_id = ClientId,
        endpoints = Endpoints,
        config = Config,
        group_id = GroupId,
        topics = TopicMap,
        callback_mod = Mod,
        callback_mod_args = ModArgs,
        group_opts = GroupOpts,
        consumer_opts = ConsumerOpts,
        producer_opts = ProducerOpts,
        subscribers = []
    },

    case start_client(St1) of
        {ok, St2} ->
            {noreply, St2};
        {error, Reason} ->
            {stop, Reason, St1}
    end;

handle_info(
    {'EXIT', Pid, max_rejoin_attempts}, #state{coordinator = Pid} = State) ->
    _ = lager:warning("Stopping; reason=coordinator down."),
    St1 = State#state{coordinator = undefined},
    true = exit(whereis(kafka_consumer_group_sup), kill),
    {stop, coordinator_down, St1};

handle_info(
    {'DOWN', _MonitorRef, process, Pid, Reason},
    #state{subscriber_id = Pid} = State) ->
    _ = lager:warning("Subscriber supervisor error; reason=~p", [Reason]),
    true = exit(whereis(kafka_consumer_group_sup), kill),
    {stop, Reason, State};

handle_info({'DOWN', _MonitorRef, process, _Pid, _Reason}, State) ->
    {noreply, State};

handle_info(Info, State) ->
    _ = lager:warning("Received unknown info message; message=~p, state=~p", [Info, State]),
    {stop, unknown_message, State}.


handle_call(Call, From, State) ->
    _ = lager:info("Received unknown call; message=~p, from:~p", [Call, From]),
    {noreply, State}.


handle_cast({assignments_received, MemberId, GenId, Assignments}, St0) ->
    _ = lager:info(
        "Stopping and re-starting subscribers; reason=assignments_received, "
        "member_id=~p, generation_id=~p",
        [MemberId, GenId]
    ),
    St1 = stop_subscribers(St0),
    St2 = St1#state{member_id = MemberId, generation_id = GenId},

    %% We first notify the subscriber Mod of assignments
    %% #state.callback_mod should imeplement the kafka_group_subscriber
    %% behaviour
    External = [
        {
            Assignment#brod_received_assignment.topic, Assignment#brod_received_assignment.partition
        }
        || Assignment <- Assignments
    ],
    %% TODO we should only be passsing the diff between the previous and
    %% current assignments
    ok = kafka_group_subscriber:handle_assignments(
        St2#state.callback_mod, External),

    {noreply, start_subscribers(Assignments, St2)};

handle_cast(assignments_revoked, St) ->
    %% TODO should'nt we be calling kafka_group_subscriber:handle_assignments
    %% passing the diff between the previous and current assignments?
    _ = lager:info("Stopping subscribers; reason=assignments_revoked", []),
    {noreply, stop_subscribers(St)};

handle_cast(Cast, State) ->
    _ = lager:info("Unknown cast message; message=~p", [Cast]),
    {noreply, State}.


code_change(_OldVsn, State, _Extra) ->
    {ok, State}.


terminate({reached_max_retries, _}, _State) ->
    _ = lager:info("Terminated; reason=reached_max_retries"),
    %% ok = kafka_group_monitor:increase_restarts(GroupId),
    true = exit(whereis(kafka_consumer_group_sup), kill),
    ok;

terminate(Reason, #state{coordinator = Pid}) when is_pid(Pid) ->
    _ = lager:info("Terminating (coordinator available); reason=~p", [Reason]),
    _ = brod_group_coordinator:commit_offsets(Pid),
    ok;

terminate(Reason, _State) ->
    _ = lager:info("Terminated; reason=~p", [Reason]),
    ok.



%% =============================================================================
%% PRIVATE
%% =============================================================================



%% @private
start_subscribers(Assignments, #state{group_id = GroupId} = St) ->
    _ = lager:info(
        "Starting subscribers for received assignments; group_id=~p",
        [GroupId]
    ),

    case supervisor:restart_child(
        {via, gproc, {n, l, GroupId}}, kafka_group_subscriber_sup)
    of
        {ok, SubscriberPid} ->
            %% Monitor supervisor!
            _ = erlang:monitor(process, SubscriberPid),

            ClientId = St#state.client_id,
            Map = fun (#brod_received_assignment{} = A) ->
                TopicName = A#brod_received_assignment.topic,
                Topic = maps:get(TopicName, St#state.topics),
                P = A#brod_received_assignment.partition,
                Offset = A#brod_received_assignment.begin_offset,
                Opts = #{
                    client_id => ClientId,
                    coordinator => St#state.coordinator,
                    generation_id => St#state.generation_id,
                    group_id => GroupId,
                    topic => Topic,
                    partition => P,
                    begin_offset => Offset,
                    callback_mod => St#state.callback_mod,
                    callback_mod_args => St#state.callback_mod_args,
                    group_opts => St#state.group_opts,
                    consumer_opts => St#state.consumer_opts,
                    producer_opts => St#state.producer_opts
                },
                {ok, Pid} = kafka_group_subscriber_sup:add_child(Opts),
                    %% We might no need to track subscribers
                    %% we need to makae acks in each one directly
                #subscriber{
                    partition = P,
                    begin_offset = Offset,
                    topic = Topic,
                    pid = Pid
                }
            end,
            St#state{
                subscriber_id = SubscriberPid,
                subscribers = lists:map(Map, Assignments)
            };
        {error, Reason} ->
            lager:error("Could not restart kafka_group_subscriber_sup, reason=~p", [Reason]),
            exit(Reason)
    end.


%% @private
stop_subscribers(#state{group_id = Id} = State) ->
    lager:info("Stop subscribers: groupId=~p", [Id]),
    ok = kafka_group_subscriber_sup:stop(Id),
    State#state{
        subscriber_id = undefined
    }.



%% =============================================================================
%% PRIVATE
%% =============================================================================



%% @private
-spec start_client(state()) -> {ok, state()} | {error, any()}.

start_client(St0) ->
    Endpoints = St0#state.endpoints,
    ClientId = St0#state.client_id,
    Config = St0#state.config,
    case brod:start_client(Endpoints, ClientId, Config) of
        ok ->
            start_stack(St0);
        {error, Reason} ->
            retry_start_client(Reason, St0)
    end.


%% @private
-spec retry_start_client(any(), state()) -> {ok, state()} | {error, any()}.

retry_start_client(Reason, #state{max_retries = M, retries = M}) ->
    lager:info( "retry_start_client: reached_max_retries" ),
    {error, {reached_max_retries, Reason}};

retry_start_client(Reason, St0) ->
    N = St0#state.retries,
    lager:info( "retry_start_client: n=~p", [N] ),

    {Time, B1} = backoff:fail(St0#state.backoff),
    lager:info(
        "Kafka connection error. "
        "Will retry after ~p msecs, reason=~p, remaining_attemps=~p",
        [Time, Reason, St0#state.max_retries - N]
    ),
    ok = timer:sleep(Time),
    start_client(St0#state{backoff = B1, retries = N + 1}).


%% @private
start_stack(St0) ->
    TopicNames = maps:keys(St0#state.topics),
    ClientId = St0#state.client_id,
    TopicNames = maps:keys(St0#state.topics),
    GroupId = St0#state.group_id,
    GroupOpts = St0#state.group_opts,
    B0 = St0#state.backoff,

    try
        {ok, CoordPid} = brod_group_coordinator:start_link(
            ClientId, GroupId, TopicNames, GroupOpts, ?MODULE, self()),
        {_, B1} = backoff:succeed(B0),
        lager:info( "start stack: coordinatorPid=~p", [CoordPid] ),
        St1 = St0#state{
            coordinator = CoordPid,
            backoff = B1
        },
        {ok, St1}
    catch
        throw:Reason ->
            retry_start_client(Reason, St0);
        _:Reason ->
            retry_start_client(Reason, St0)
    end.

