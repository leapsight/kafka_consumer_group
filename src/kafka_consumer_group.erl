%% -----------------------------------------------------------------------------
%% @doc
%% ## Quarantine Topic
%% The purpose of the Quarantine Topic (QT) is to hold messages that could not
%% be processed. Those messages can then be consumed from the QT for
%% inspection. An application might, with help of an operator, correct issues
%% and reprocess the message, log the fact that there was an error, and take
%% corrective action.
%%
%% See how it works in {@link kafka_group_subscriber:handle_message/5} callback.
%% @end
%% -----------------------------------------------------------------------------
-module(kafka_consumer_group).


-type topic_spec() :: #{
    name => brod:topic(),
    max_retries => non_neg_integer(),
    quarantine_topic_name => brod:topic(),
    quarantine_message_formatter => quarantine_message_formatter()
}.

%% Follows kafka_group_member:opts() replacing topics => brod:topics()
-type opts()        ::  #{
    client_id => atom(),
    group_id => binary(),
    topics => [topic_spec()],
    callback_mod => module(),
    callback_mod_args => any(),
    consumer_opts => map(),
    producer_opts => map(),
    group_opts => map()
}.

-type quarantine_message_formatter() :: fun(
    (GroupId :: binary(), brod:topic(), brod:message(), Reason :: any()) ->
        {brod:partition() | brod:partition_fun(), brod:key(), brod:value()}
).

-export_type([topic_spec/0]).
-export_type([opts/0]).
-export_type([quarantine_message_formatter/0]).

-export([start_group/1]).
-export([restart_group/1]).
-export([stop_group/1]).



%% =============================================================================
%% API
%% =============================================================================


%% -----------------------------------------------------------------------------
%% @doc
%% Starts a group consumer instance i.e. an instance of the kafka_group_sup
%% its child tree containing a subscriber per topic partition asigned to this
%% instance.
%%
%% Opts is a map containing the following keys:
%% * client_id (required, atom) - brod client ID
%% * group_id (required, binary) -
%% * topics (required) - the list of topic specifications ( `topic_spec()' )
%% * callback_mod (required, module) - the callback module that handles the
%% messages
%% * callback_mod_args (required, map)- a map that will be passed to the
%% callback Mod init function, if missing the empty map will be passed.
%% * group_opts (optional) - brod group opts,
%% * consumer_opts (optional) - brod consumer opts,
%% * producer_opts  (optional) - brod producer opts
%%
%% @end
%% -----------------------------------------------------------------------------
-spec start_group(opts()) -> {ok, pid()} | {error, any}.
start_group(Opts) ->
    kafka_consumer_group_sup:start_group(Opts).


-spec restart_group(binary()) -> {ok, pid()} | {error, any}.
restart_group(GroupId) ->
    kafka_consumer_group_sup:restart_group(GroupId).


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
stop_group(GroupId) ->
    kafka_consumer_group_sup:stop_group(GroupId).