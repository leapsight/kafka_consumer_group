%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-module(kafka_group_subscriber_sup).
-behaviour(supervisor).

-define(SUPERVISOR(X), {via, gproc, {n, l, {X, ?MODULE}}}).

%% API
-export([start_link/1]).
-export([stop/1]).
-export([add_child/1]).

%% Supervisor callbacks
-export([init/1]).



%% =============================================================================
%% API
%% =============================================================================



%% -----------------------------------------------------------------------------
%% @doc
%% Starts a new supervisor instance
%% @end
%% -----------------------------------------------------------------------------
-spec start_link(GroupId :: binary()) ->
    {ok, pid()}
    | ignore
    | {error, {already_started, pid()} | {shutdown, term()} | term()}.

start_link(GroupId) when is_binary(GroupId) ->
    supervisor:start_link(?SUPERVISOR(GroupId), ?MODULE, []).


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec stop(atom()) -> ok | {error, not_found}.

stop(GroupId) when is_binary(GroupId) ->
    supervisor:terminate_child(
        {via, gproc, {n, l, GroupId}}, kafka_group_subscriber_sup).


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec add_child(kafka_group_subscriber:opts()) -> {ok, pid()} | {error, any()}.

add_child(#{group_id := GroupId} = Opts) ->
    supervisor:start_child(?SUPERVISOR(GroupId), [Opts]).



%% =============================================================================
%% SUPERVISOR CALLBACKS
%% =============================================================================




init([]) ->
    SupFlags = #{
        strategy => simple_one_for_one,
        intensity => 200,
        period => 2
    },
    Spec = #{
        id => kafka_group_subscriber,
        start => {
            kafka_group_subscriber,
            start_link,
            []
        },
        restart => transient,
        shutdown => 5000,
        type => worker,
        modules => [kafka_group_subscriber]
    },
    lager:debug("Starting ~p", [?MODULE]),
    {ok, {SupFlags, [Spec]}}.





%% =============================================================================
%% PRIVATE
%% =============================================================================







