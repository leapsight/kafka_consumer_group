%% -----------------------------------------------------------------------------
%% @doc
%% This module implements the  Kafka Consumer Group application top level
%% supervisor.
%%
%% The following diagram describes the supervision tree.
%%
%% <pre>
%%
%% +-----------------------------+
%% |                             |
%% |  kafka_consumer_group_sup   |
%% |   one_for_one, transient    |
%% |                             |
%% +-----------------------------+
%%                |
%%               /|\
%% +-----------------------------+
%% |                             |
%% |       kafka_group_sup       |
%% |   one_for_all, transient    |
%% |                             |
%% +-----------------------------+
%%                |
%%                +---------------------------------+
%%                -                                 -
%%                |                                 |
%% +-----------------------------+   +-----------------------------+
%% |                             |   |                             |
%% |     kafka_group_member      |   | kafka_group_subscriber_sup  |
%% |                             |   |simple_one_for_one, transient|
%% |                             |   |                             |
%% +-----------------------------+   +-----------------------------+
%%                |                                 |
%%                |                                /|\
%%                |                  +-----------------------------+
%%                |                  |                             |
%%                |                  |   kafka_group_subscriber    |
%%                |                  |                             |
%%                |                  +-----------------------------+
%%                |                                 |
%%                -                                 -
%%                |                                 |
%% +-----------------------------+   +-----------------------------+
%% |                             |   |                             |
%% |      brod_coordinator       |   |        brod_consumer        |
%% |                             |   |                             |
%% +-----------------------------+   +-----------------------------+
%%                                                  |
%%                                                  -
%%                                                  |
%%                                   +-----------------------------+
%%                                   |                             |
%%                                   |       kafka partition       |
%%                                   |                             |
%%                                   +-----------------------------+
%%
%% </pre>
%% @end
%% -----------------------------------------------------------------------------
-module(kafka_consumer_group_sup).
-behaviour(supervisor).


%% API
-export([start_link/0]).
-export([start_group/1]).
-export([restart_group/1]).
-export([stop_group/1]).


%% Supervisor callbacks
-export([init/1]).




%% =============================================================================
%% API
%% =============================================================================



%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).


%% -----------------------------------------------------------------------------
%% @doc
%% Starts a group consumer instance i.e. an instance of the kafka_group_sup
%% its child tree containing a subscriber per topic partition asigned to this
%% instance.
%%
%% Opts is a map containing the following keys:
%% * client_id (required, atom) - brod client ID
%% * group_id (required, binary) -
%% * topics (required) - the list of topic specifications
%% * callback_mod (required, module) - the callback module that handles the messages
%% * callback_mod_args (required, map)- a map that will be passed to the callback Mod init function, if missing the empty map will be passed.
%% * group_opts (optional) - brod group opts,
%% * consumer_opts (optional) - brod consumer opts,
%% * producer_opts  (optional) - brod producer opts,
%% @end
%% -----------------------------------------------------------------------------
-spec start_group(kafka_group_member:opts()) -> {ok, pid()} | {error, any}.

start_group(Opts) ->
    #{
        client_id := _ClientId,
        group_id := GroupId,
        topics := _,
        callback_mod := _
    } = Opts,

    ChildSpec = child_spec(GroupId, Opts),
    case supervisor:start_child(?MODULE, ChildSpec) of
        {ok, _} = OK ->
            ok = kafka_group_monitor:add_group(GroupId, 5),
            OK;
        {error, already_present} ->
            restart_group(GroupId);
        Other ->
            Other
    end.


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec restart_group(binary) -> {ok, pid()} | {error, any}.

restart_group(GroupId) ->
    supervisor:restart_child(?MODULE, GroupId).


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
stop_group(GroupId) ->
    kafka_group_sup:stop(GroupId).



%% =============================================================================
%% SUPERVISOR CALLBACKS
%% =============================================================================




init([]) ->
    SupFlags = #{
        strategy => one_for_one,
        intensity => 1,
        period => 5
    },
    Children = [
        #{
            id => kafka_group_monitor,
            start => {
                kafka_group_monitor,
                start_link,
                []
            },
            restart => permanent,
            shutdown => 5000,
            type => worker,
            modules => [kafka_group_monitor]
        }
    ],
    {ok, {SupFlags, Children}}.





%% =============================================================================
%% PRIVATE
%% =============================================================================


%% @private
child_spec(GroupId, Opts) ->
    #{
        id => GroupId,
        start => {
            kafka_group_sup,
            start_link,
            [Opts]
        },
        %% restart => transient,
        restart => permanent,
        shutdown => infinity,
        type => supervisor,
        modules => [kafka_group_sup]
    }.
