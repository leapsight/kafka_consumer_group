%% -----------------------------------------------------------------------------
%% @doc
%% This module defines the kafka_group_subscriber
%%
%% ## Callbacks
%% The user module should export:
%%
%% <pre>
%% init(Args) ==> {ok, CallbackState}
%% </pre>
%%
%% <pre>
%% handle_message(Topic, Partition, Message, Subscriber, CallbackState)
%%  ==> {no_ack, NewCallbackState}
%%      | {ack, NewCallbackState}
%%      | {retry, NewCallbackState}
%%      | {async_retry, NewCallbackState}.
%%      | {error, overload, NewCallbackState}.
%%      | {error, Reason, NewCallbackState}.
%% </pre>
%%
%% Callback for a handler to process an individual kafka message.
%% Must return one of:
%%
%% * `{no_ack, NewCallbackState}' - the callback handler takes responsibility of
%% asynchronously processing the message and acknowledging the offset later on
%% by calling {@link kafka_group_subcriber:ack/2}. The handler is ready to
%% process the next message.
%% * `{ack, NewCallbackState}'  - the handler has completed processing
%% the message, the offset is committed and the handler is ready to process the
%% next message.
%% * `{retry, NewCallbackState}' - pauses the polling of messages from kafka
%% (temporarily unsubscribing from the brod_consumer, storing the message in a
%% local in-memory buffer and once the remaining messages in the set have been
%% processed, retries processing the enqueued messages N times each using an
%% exponential backoff strategy. Any message that could not be processed after
%% the max_retries has been reached is written in the provided Quarantine Topic
%% (QT) or discarded if one was not configured. If the application dies while
%% retrying, the buffer is lost.
%% * `{async_retry, NewCallbackState} '- we enqueue the message in a Retry
%% Queue (RQ) acknowledging it, while handling the retries
%% asynchronously and continuing processing the next message. The RQ is stored
%% in memory and/or in disk and can use a linear or exponential backoff
%% strategy. Also, in order to recover from a node failure the RQ can be
%% recreated at startup from a RQ Change Log that we generate by writing
%% operations into a Kafka Topic with compaction enabled. After a number
%% (max\_retries) of retries the message is dimmed unrecoverable and written to
%% a Quarantine Topic (also known as a Dead Letter Topic or Queue) if one was configured. The QT is defined in the topic specification of each topic provided
%% when starting a consumer group.
%% * `{error, overload, NewCallbackState}' - the handler cannot process more
%% messsages and wants to temporarily pause consuming messages.
%% The subscriber waits a period of time using an exponential backoff strategy
%% before calling the handler again with the last unprocessed message.
%%* `{error, Reason, NewCallbackState}` - if a QT
%% was configured it writes the message in the QT - the reason
%% is passed as an argument to the `quarantine_message_formatter'. Otherwise,
%% acknowledges the message (committing the offset) and continues processing
%% the next message.
%%
%% While this callback function is being evaluated, the `brod' fetch-ahead
%% partition-consumers are fetching more messages behind the scene
%% unless `prefetch_count' is set to 0 in the `consumer_opts' config of the
%% consumer group (See {@link kafka_consumer_group:start_group/1}).
%%
%% @end
%% -----------------------------------------------------------------------------
-module(kafka_group_subscriber).
-behaviour(gen_server).
-include_lib("brod/include/brod.hrl").


-define(RETRY_LIMIT, 5).
-define(RETRY_BACKOFF_SECS, 3).

-record(state, {
    group_id                    ::  binary(),
    consumer                    ::  pid() | undefined,
    client                      ::  pid(),
    coordinator                 ::  pid(),
    generation_id               ::  brod:generation_id(),
    topic                       ::  brod:topic(),
    max_retries                 ::  non_neg_integer(),
    quarantine_topic            ::  brod:topic() | undefined,
    qt_message_formatter        ::  qt_message_formatter(),
    partition                   ::  brod:partition(),
    consumer_opts               ::  map(),
    begin_offset                ::  brod:offset_time(),
    high_wm_offset              ::  brod:offset(),
    acked_offset                ::  brod:offset(),
    %% pending_acks = []           ::  list(),
    is_suspended = false        ::  boolean(),
    backoff_max = 60000         ::  pos_integer() | infinity,
    backoff                     ::  any() | undefined,
    backoff_timer               ::  reference(),
    callback_mod                ::  module(),
    callback_mod_args           ::  any(),
    callback_mod_states = #{}   ::  map()
}).

-type state()                   ::  #state{}.

-type qt_message_formatter() ::
    kafka_consumer_group:quarantine_message_formatter()
    | undefined.

-type opts()        ::  #{
    client_id => atom(),
    coordinator => pid(),
    generation_id => brod:group_generation_id(),
    group_id => binary(),
    topic => kafka_consumer_group:topic_spec(),
    partition => brod:partition(),
    begin_offset => brod:offset_time(),
    callback_mod => module(),
    callback_mod_args => any(),
    consumer_opts => map(),
    producer_opts => map(),
    group_opts => map()
}.

-type assignments() :: [{Topic :: binary(), Partition :: integer()}].

-export_type([opts/0]).
-export_type([assignments/0]).


%% API
-export([start_link/1]).
-export([ack/2]).
-export([commit_offsets/1]).
-export([commit_offsets/2]).
-export([handle_assignments/2]).

%% GEN_SERVER CALLBACKS
-export([code_change/3]).
-export([handle_call/3]).
-export([handle_cast/2]).
-export([handle_info/2]).
-export([init/1]).
-export([terminate/2]).



%% =============================================================================
%% CALLBACKS
%% =============================================================================



%% -----------------------------------------------------------------------------
%% This function is used to initialize the message handler.
%% -----------------------------------------------------------------------------
-callback init(Args :: any()) ->
    {ok, CallbackState :: any()}.


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-callback handle_message(
    brod:topic(),
    brod:partition(),
    brod:message(),
    Subscriber :: pid(),
    State :: any()) ->
    {no_ack, NewState :: any()}
    | {ack, NewState :: any()}
    | {retry, NewState :: any()}
    | {async_retry, NewState :: any()}
    | {error, overload | any(), NewState :: any()}.


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-callback terminate(CallbackState :: any()) -> ok.
-optional_callbacks([terminate/1]).


%% -----------------------------------------------------------------------------
%% @doc You can use this callback to for example perform any state cleanup
%% based on the {topic, partition} tuples.
%% This should be a blocking call since kafka_group_member will then
%% stop/re-start subscribers right after.
%% @end
%% -----------------------------------------------------------------------------
-callback handle_assignments(assignments()) -> ok.
-optional_callbacks([handle_assignments/1]).



%% =============================================================================
%% API
%% =============================================================================



%% -----------------------------------------------------------------------------
%% @doc
%% ssss
%% @end
%% -----------------------------------------------------------------------------
start_link(Opts) ->
    gen_server:start_link(?MODULE, [Opts], []).


%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-spec ack(Subscriber :: pid(), Offset :: brod:offset()) -> ok.

ack(Subscriber, Offset) ->
    gen_server:cast(Subscriber, {ack, Offset}).


%% -----------------------------------------------------------------------------
%% @doc
%% Force commit collected (acked) offsets immediately.
%% @end
%% -----------------------------------------------------------------------------
-spec commit_offsets(Subscriber :: pid()) -> ok.

commit_offsets(Subscriber) ->
    gen_server:cast(Subscriber, commit_offsets).


%% -----------------------------------------------------------------------------
%% @doc
%% Force commit collected (acked) offsets plus the given extra offsets
%% immediately.
%% NOTE: A lists:usrot is applied on the given extra offsets to commit
%%      meaning if two or more offsets for the same topic-partition exist
%%      in the list, only the one that is closer the head of the list is kept
%% @end
%% -----------------------------------------------------------------------------
-spec commit_offsets(Subscriber :: pid(), Offsets :: [brod:offset()]) -> ok.

commit_offsets(Subscriber, Offsets) ->
    gen_server:cast(Subscriber, {commit_offsets, Offsets}).


%% -----------------------------------------------------------------------------
%% @doc This function is called by kafka_group_member every time there is
%% a new kafka topic partition assignment.
%% This should be a blocking call since kafka_group_member will then
%% stop/re-start subscribers right after.
%% @end
%% -----------------------------------------------------------------------------
-spec handle_assignments(CallbackMod :: module(), assignments()) -> ok.

handle_assignments(Mod, Assignments) ->
    _ = lager:debug(
        "Received kafka partition assignments; assignments=~p ", [Assignments]
    ),
    case erlang:function_exported(Mod, handle_assignments, 1) of
        true ->
            Mod:handle_assignments(Assignments);
        false ->
            _ = lager:info(
                "handle_assignments not exported by group subscriber; "
                "subscriber=~p",
                [Mod, Assignments]
            ),
            ok
    end.



%% =============================================================================
%% OTP CALLBACKS
%% =============================================================================



init([Opts])  ->
    Offset = maps:get(begin_offset, Opts),
    Topic = maps:get(topic, Opts),
    CBMod = maps:get(callback_mod, Opts),

    State = #state{
        group_id = maps:get(group_id, Opts),
        client = maps:get(client_id, Opts),
        coordinator = maps:get(coordinator, Opts),
        generation_id = maps:get(generation_id, Opts),
        topic = maps:get(name, Topic),
        max_retries = maps:get(max_retries, Topic),
        quarantine_topic = maps:get(
            quarantine_topic_name, Topic, undefined),
        qt_message_formatter = maps:get(
            quarantine_message_formatter, Topic, undefined),
        partition = maps:get(partition, Opts),
        consumer_opts = maps:get(consumer_opts, Opts),
        begin_offset = Offset,
        callback_mod = CBMod,
        callback_mod_args = maps:get(callback_mod_args, Opts)
    },
    self() ! {subscribe, 0},
    {ok, State}.


handle_info({subscribe, Count}, State0) ->
    {noreply, subscribe(Count, State0)};

handle_info({Pid, #kafka_message_set{} = Set}, #state{consumer = Pid} = St) ->
    {noreply, handle_message_set(Set, St)};

handle_info(
    {Pid, #kafka_fetch_error{} = Error}, #state{consumer = Pid} = State) ->
    _ = lager:warning("Re-subscribing due to kafka error; reason=~p", [Error]),
    {noreply, subscribe(5, State)};

handle_info({'DOWN', _MonitorRef, process, Pid, Reason}, #state{consumer = Pid} = State) ->
    _ = lager:warning(
        "Re-subscribing due to consumer exit; reason=~p", [Reason]),
    {noreply, subscribe(5, State)};

handle_info(Info, State) ->
    lager:warning("Received unknown info message; message=~p", [Info]),
    {noreply, State}.


handle_call(Call, From, State) ->
    _ = lager:warning("Received unknown call; message=~p, from=~p", [Call, From]),
    {noreply, State}.


handle_cast({ack, Offset}, State) ->
    {noreply, handle_ack(Offset, State)};

handle_cast(commit_offsets, State) ->
    ok = brod_group_coordinator:commit_offsets(State#state.coordinator),
    {noreply, State};

handle_cast({commit_offsets, L}, State) ->
    T = State#state.topic,
    P = State#state.partition,
    ok = brod_group_coordinator:commit_offsets(
        State#state.coordinator, [{{T, P}, L}]),
    {noreply, State};

handle_cast(Cast, State) ->
    _ = lager:warning("Unknown cast message; message=~p", [Cast]),
    {noreply, State}.


code_change(_OldVsn, State, _Extra) ->
    {ok, State}.


terminate(_Reason, State) ->
    Mod = State#state.callback_mod,
    case erlang:function_exported(Mod, terminate, 1) of
        true ->
            ok = Mod:terminate(mod_state(Mod, State)),
            ok;
        false ->
            ok
    end.



%% =============================================================================
%% PRIVATE
%% =============================================================================



%% @private
subscribe(RetryCount, State0) ->
    #state{
        client = Client,
        topic = Topic,
        partition = Partition,
        consumer_opts = ConsumerOpts,
        begin_offset = BeginOffset
    } = State0,

    %% Start consumer (if necesary)
    ok = brod:start_consumer(Client, Topic, ConsumerOpts),

    %% Subscribe to topic
    Opts = resolve_opts(State0),
    _ = lager:info(
        "Subscribing; topic=~p, partition=~p, begin=~p, opts=~p",
        [Topic, Partition, BeginOffset, Opts]
    ),
    case brod:subscribe(Client, self(), Topic, Partition, Opts) of
        {ok, Pid} ->
            _ = lager:debug(
                "Subscribed; topic=~p, partition=~p, consumer_pid=~p",
                [Topic, Partition, Pid]
            ),
            _ = erlang:monitor(process, Pid),
            State0#state{consumer = Pid, is_suspended = true};
        {error, Reason} when RetryCount < ?RETRY_LIMIT ->
            _ = lager:debug(
                "Subscribe error; "
                "reason=~p, topic=~p, partition=~p, retry_count=~p",
                [Reason, Topic, Partition, RetryCount]
            ),
            handle_subscribe_error(Client, Topic, Partition, Reason),
            Msg = {subscribe, RetryCount + 1},
            erlang:send_after(timer:seconds(?RETRY_BACKOFF_SECS), self(), Msg),
            State0;
        {error, Reason} ->
            _ = lager:warning(
                "Subscribe error; reason=~p, topic=~p, partition=~p",
                [Reason, Topic, Partition]
            ),
            exit({failed_to_subscribe, Reason})
    end.

handle_subscribe_error(Client, Topic, Partition, Reason) ->
    case Reason of
        'NotLeaderForPartition' ->
            {ok, ConsumerPid} = brod_client:get_consumer(
                Client, Topic, Partition
            ),
            exit(ConsumerPid, <<"Invalid leader state">>),
            ok;
        _ ->
            ok
    end.


%% @private
resolve_opts(#state{begin_offset = BeginOffset}=State)
when is_integer(BeginOffset), BeginOffset >= -2 ->
    resolve_begin_offset(State);

resolve_opts(#state{begin_offset = undefined}=State) ->
    resolve_begin_offset(State);

resolve_opts(#state{begin_offset = BeginOffset}) ->
    [{begin_offset, BeginOffset}].


%% @private
resolve_begin_offset(State) ->
    #state{
        group_id = GroupId,
        client = Client,
        topic = Topic,
        partition = Partition,
        consumer_opts = ConsumerOpts
    } = State,
    case get_commited_offset(GroupId, Client, Topic, Partition) of
        {ok, Offset} when Offset >= 0 ->
            [{begin_offset, Offset}];
        _ ->
            case lists:keyfind(begin_offset, 1, ConsumerOpts) of
                {begin_offset, BeginOffset} -> [{begin_offset, BeginOffset}];
                _ -> []
            end
    end.


get_commited_offset(GroupId, Client, Topic, Partition) ->
    try
        %% Get commit offsets
        {ok, [TopicPartitionsOffset]} = brod_utils:fetch_committed_offsets(
            Client,
            GroupId,
            [{Topic, [Partition]}]
        ),

        %% Check topic
        Topic = proplists:get_value(topic, TopicPartitionsOffset, undefined),

        %% Get offset
        PartitionOffsets = proplists:get_value(
            partition_responses, TopicPartitionsOffset, []),
        lists:foldl(
            fun([{partition, PartitionItem}, {offset, Offset}, {metadata, _}, {error_code, Error}], AccIn) ->
                case PartitionItem =:= Partition andalso Error =:= no_error of
                    true -> {ok, Offset};
                    _ -> AccIn
                end
            end,
            {error, not_found},
            PartitionOffsets
        )
    catch
        _:Reason ->
            lager:warning(
                "Error while getting commited offset; "
                "group_id=~p, topic=~p, partition=~p, error=~p",
                [GroupId, Topic, Partition, Reason]
            ),
            {error, Reason}
    end.


%% @private
handle_message_set(Set, State0) ->
    #kafka_message_set{
        topic = T,
        partition = P,
        high_wm_offset = O,
        messages = Messages
    } = Set,

    Mod = State0#state.callback_mod,
    Map = State0#state.callback_mod_states,
    %% We init the processor module state in case it wasn't yet
    State1 = case maps:is_key(Mod, Map) of
        true ->
            State0;
        false ->
            {ok, ModState} = Mod:init(State0#state.callback_mod_args),
            update_mod_state(Mod, ModState, State0)
    end,
    handle_messages(Messages, T, P, Mod, State1#state{high_wm_offset = O}).



%% -----------------------------------------------------------------------------
%% @private
%% @doc
%% Unsubscribes from consumer, making the consumer stop pulling from Kafka.
%% Schedules a re-subscription using exponential backoff
%% @end
%% -----------------------------------------------------------------------------
backoff(#state{backoff = undefined} = State) ->
    case brod:unsubscribe(self()) of
        ok ->
            _ = lager:info("Unsubscribed; reason=overload", []),
            B0 = backoff:init(1000, State#state.backoff_max, self(), subscribe),
            B1 = backoff:type(B0, jitter),
            State#state{
                backoff = B1,
                backoff_timer = backoff:fire(B1),
                is_suspended = true
            };
        {error, Reason} ->
            _ = lager:error("Error while unsubscribing; reason=~p", [Reason]),
            error(Reason)
    end;

backoff(#state{backoff = B0} = State) ->
    case backoff:fail(B0) of
        {T, _} when T > State#state.backoff_max ->
            _ = lager:error(
                "Backoff reached limit; reason=overload, limit=~p",
                [State#state.backoff_max]
            ),
            error(overload);
        {T, B1} ->
            case brod:unsubscribe(self()) of
                ok ->
                    _ = lager:info(
                        "Unsubscribed. Will re-subscribe in ~p milliseconds; reason=overload",
                        [T]
                    ),
                    State#state{
                        backoff = B1,
                        backoff_timer = backoff:fire(B1),
                        is_suspended = true
                    };
                {error, Reason} ->
                    lager:error(
                        "Error while unsubscribing; reason=~p", [Reason]),
                    error(Reason)
            end
    end.


%% @private
reset_backoff(#state{backoff = undefined} = State) ->
    State;

reset_backoff(State) ->
    _ = (catch erlang:cancel_timer(State#state.backoff_timer)),
    State#state{
        backoff = undefined,
        backoff_timer = undefined
    }.



%% -----------------------------------------------------------------------------
%% @doc
%% -record(kafka_message,
%% { offset :: kpro:offset()
%% , magic_byte :: kpro:int8()
%% , attributes :: kpro:int8()
%% , key :: kpro:bytes()
%% , value :: kpro:bytes()
%% , crc :: non_neg_integer() %% not kpro:int32() because it's unsigned
%% , ts_type :: kpro:timestamp_type()
%% , ts :: kpro:int64()
%% }).
%% Kakfa 0.10.1 adds a time-based index. It allows to seek to the first record with a timestamp equals or larger of the given timestamp. You can use it via KafkaConsumer#offsetsForTime(). This will return the corresponding offsets and you can feed them into KafkaConsumer#seek(). You can just consume the data and check the records timestamp field via ConsumerRecord#timestamp() to see when you can stop processing.
%% But this is not yet supported by brod
%% @end
%% -----------------------------------------------------------------------------
handle_messages([#kafka_message{} = H|T], Topic, Part, Mod, State0) ->
    _ = lager:debug("Handling message; message=~p", [H]),
    case Mod:handle_message(Topic, Part, H, self(), mod_state(Mod, State0)) of
        {noack, ModState} ->
            State1 = reset_backoff(update_mod_state(Mod, ModState, State0)),
            handle_messages(T, Topic, Part, Mod, State1);
        {ack, ModState} ->
            Offset = H#kafka_message.offset,
            State1 = reset_backoff(update_mod_state(Mod, ModState, State0)),
            State2 = handle_ack(Offset, State1),
            handle_messages(T, Topic, Part, Mod, State2);
        {retry, _ModState} ->
            error(not_implemented_yet);
        {async_retry, _ModState} ->
            error(not_implemented_yet);
        {error, overload, ModState} ->
            backoff(update_mod_state(Mod, ModState, State0));
        {error, Reason, ModState} ->
            Offset = H#kafka_message.offset,
            State1 = reset_backoff(update_mod_state(Mod, ModState, State0)),
            State2 = handle_ack(Offset, State1),
            ok = maybe_write_to_qt(H, Reason, State2),
            handle_messages(T, Topic, Part, Mod, State2)
    end;

handle_messages([], _, _, _, State) ->
    State.


%% @private
handle_ack(Offset, #state{} = St0) ->
    %% Tell consumer process to fetch more (if pre-fetch count allows).
    ok = brod:consume_ack(St0#state.consumer, Offset),
    %% Commit offset
    ok = brod_group_coordinator:ack(
        St0#state.coordinator,
        St0#state.generation_id,
        St0#state.topic,
        St0#state.partition,
        Offset
    ),
    St0#state{acked_offset = Offset}.


%% @private
mod_state(Mod, #state{callback_mod_states = Map}) ->
    maps:get(Mod, Map).


%% @private
update_mod_state(Mod, ModState, #state{callback_mod_states = Map} = State0) ->
    State0#state{callback_mod_states = maps:put(Mod, ModState, Map)}.


%% @private
maybe_write_to_qt(H, Reason0, State) ->
    case State#state.quarantine_topic of
        undefined ->
            ok;
        QT ->
            Client = State#state.client,
            {PartOrFun, Key, Value} = format_qt_message(H, Reason0, State),
            case brod:produce_sync(Client, QT, PartOrFun, Key, Value) of
                ok ->
                    ok;
                {error, Reason1} ->
                    _ = lager:error(
                        "Error producing to quarantine topic; "
                        "topic=~p, reason=~p",
                        [QT, Reason1]
                    ),
                    ok
            end
    end.


%% private
-spec format_qt_message(brod:message(), any(), state()) ->
    {brod:partition() | brod:prodfun(), brod:key(), brod:value()}.

format_qt_message(M, Reason, State) ->
    Fun = State#state.qt_message_formatter,
    Fun(State#state.group_id, State#state.topic, M, Reason).
