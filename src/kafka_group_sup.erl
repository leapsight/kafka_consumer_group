%% -----------------------------------------------------------------------------
%% @doc
%% @end
%% -----------------------------------------------------------------------------
-module(kafka_group_sup).
-behaviour(supervisor).

-define(SUPERVISOR(X), {via, gproc, {n, l, X}}).

%% API
-export([start_link/1]).
-export([stop/1]).



%% Supervisor callbacks
-export([init/1]).




%% =============================================================================
%% API
%% =============================================================================



%% -----------------------------------------------------------------------------
%% @doc
%% * ClientId - Client ID of the brod client.
%% * GroupId: - Consumer group ID which should be unique per kafka cluster
%% * Topics - Predefined set of topic names to join the group.
%% * callback_mod - Callback module which should have the callback functions
%%   implemented for message processing.
%% * callback_mod_args (required, map)
%% @end
%% -----------------------------------------------------------------------------
start_link(#{group_id := GroupId} = Opts) ->
    supervisor:start_link(?SUPERVISOR(GroupId), ?MODULE, Opts).


%% -----------------------------------------------------------------------------
%% @doc
%% Terminates the supervisor and all its children
%% @end
%% -----------------------------------------------------------------------------
stop(GroupId) ->
    %% We ask our supervisor to kill us.
    Pid = gproc:lookup_local_name(GroupId),
    ok = kafka_group_monitor:demonitor_me(Pid),
    supervisor:terminate_child(kafka_consumer_group_sup, GroupId).



%% =============================================================================
%% SUPERVISOR CALLBACKS
%% =============================================================================



init(Opts) ->
    #{
        client_id := _ClientId,
        group_id := GroupId,
        topics := _,
        callback_mod := _,
        callback_mod_args := _
    } = Opts,
    ok = kafka_group_monitor:monitor_me(self()),
    SupFlags = #{
        %% If any one child (kafka_group_member or kafka_group_subscriber_sup)
        %% terminates, then terminate all children and restart
        strategy => one_for_all,
        intensity => 1,
        period => 5
    },
    ChildSpecs = [
        #{
            id => kafka_group_subscriber_sup,
            start => {
                kafka_group_subscriber_sup,
                start_link,
                [GroupId]
            },
            restart => transient,
            shutdown => infinity,
            type => supervisor,
            modules => [kafka_group_subscriber_sup]
        },
        #{
            id => kafka_group_member,
            start => {
                kafka_group_member,
                start_link,
                [Opts]
            },
            restart => transient,
            shutdown => 15000,
            type => worker,
            modules => [kafka_group_member]
        }
    ],
    {ok, {SupFlags, ChildSpecs}}.

