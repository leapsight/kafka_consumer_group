%%%-------------------------------------------------------------------
%% @doc
%% kafka_consumer_group public API
%% @end
%%%-------------------------------------------------------------------
-module(kafka_consumer_group_app).

-behaviour(application).

%% Application callbacks
-export([start/2, stop/1]).

%%====================================================================
%% API
%%====================================================================

start(_StartType, _StartArgs) ->
    lager:info( "start" ),
    %% Terminate node when brod terminates

    ok = start_brod(),

    case kafka_consumer_group_sup:start_link() of
        {ok, _} = OK ->
            %% Any additional init goes here
            OK;
        {error, _} = Error ->
            Error
    end.


stop(_State) ->
    lager:info( "stop" ),
    ok.





%% =============================================================================
%% PRIVATE
%% =============================================================================

start_brod() ->
    {ok, _} = application:ensure_all_started(brod, permanent),
    ok.

%% start_brod() ->
%%     case application:get_env(kafka_consumer_group, kafka) of
%%         {ok, PL} ->
%%             application:unload(brod),
%%             _ = [application:set_env(brod, K, V) || {K, V} <- PL],
%%             lager:debug("Brod conf ~p", [application:get_all_env(brod)]),
%%             application:load(brod),
%%             {ok, _} = application:ensure_all_started(brod, permanent),
%%             is_pid(hd(brod_sup:find_client(kafka_client_1))) orelse
%%             erlang:exit(no_client),
%%             ok;
%%         undefined ->
%%             erlang:exit(invalid_kafka_config)
%%     end.

