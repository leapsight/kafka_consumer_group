
{erl_opts, [
    warn_export_all,
    strict_validation,
    warn_bif_clash,
    warn_deprecated_function,
    % warn_export_all,
    warn_export_vars,
    warn_exported_vars,
    warn_format,
    % warn_missing_spec,
    warn_obsolete_guard,
    warn_shadow_vars,
    % warn_untyped_record,
    warn_unused_import,
    warn_unused_function,
    warn_unused_record,
    warn_unused_vars,
    warnings_as_errors,
    debug_info,
    {parse_transform, lager_transform}
]}.

{deps, [
    {utils,
        {git, "git@gitlab.com:leapsight/utils.git", {branch, "develop"}}},
    {brod, {git, "https://github.com/klarna/brod.git", {tag, "3.5.2"}}},
    gproc,
    {backoff, {git, "https://github.com/ferd/backoff.git", {branch, "master"}}},
    {lager, "~>3.2.4"}
]}.


{profiles, [
    {lint,  [
        {plugins, [
            {rebar3_lint,
                {git, "https://github.com/project-fifo/rebar3_lint.git", {tag, "0.1.2"}}
            }
        ]}
    ]},
    {docs, [
        {deps, [
            {edown,
                ".*",
                {git, "https://github.com/uwiger/edown.git", {branch, "master"}}
            }
        ]},
        {edoc_opts, [
            {doclet, edown_doclet},
            {top_level_readme, {
                "./README.md",
                "https://gitlab.com/leapsight/kafka_consumer_group"
            }}
        ]}
    ]},
    {test, [
        {sys_config, "./config/test/sys.config"},
        {deps, [
            meck,
            msgpack,
            {hash,
                {git, "https://github.com/fogfish/hash.git", {branch, "master"}}
            }
        ]},
        {erl_opts, [debug_info, nowarn_export_all]}
    ]}
]}.

%% =============================================================================
%% Shell
%% =============================================================================



{plugins, [rebar3_auto]}.
{shell, [
    {apps, [
        sasl,
        lager,
        brod,
        gproc,
        utils,
        lager,
        backoff,
        kafka_consumer_group
    ]}
]}.



%% =============================================================================
%% TESTING
%% =============================================================================

{cover_enabled, true}.
{cover_opts, [verbose]}.
{ct_opts, [
    {sys_config, "./config/test/sys.config"}
]}.

%% =============================================================================
%% QUALITY ASSURANCE TOOLS
%% =============================================================================

{dialyzer,
[{warnings,
  [race_conditions,
   %%no_return,
   unmatched_returns,
   error_handling
  ]}
 %%{get_warnings, true},
 %%{plt_apps, top_level_deps},
 %%{plt_extra_apps, []},
 %%{plt_location, local}
 %% {base_plt_apps, [stdlib, kernel]},
 %% {base_plt_location, global}
]}.

{elvis,
[#{dirs => ["src"],
filter => "*.erl",
rules => [
          %% {elvis_style, line_length,
          %%  #{ignore => [],
          %%    limit => 80,
          %%    skip_comments => false}},
          {elvis_style, no_tabs},
          {elvis_style, no_trailing_whitespace},
          {elvis_style, macro_names, #{ignore => []}},
          %% {elvis_style, macro_module_names},
          {elvis_style, operator_spaces, #{rules => [{right, ","},
                                                     {right, "++"},
                                                     {left, "++"}]}},
          %% {elvis_style, nesting_level, #{level => 3}},
          {elvis_style, god_modules,
           #{limit => 25,
             ignore => []}},
          {elvis_style, no_if_expression},
          %% {elvis_style, invalid_dynamic_call, #{ignore => []}},
          {elvis_style, used_ignored_variable},
          {elvis_style, no_behavior_info},
          {
            elvis_style,
            module_naming_convention,
            #{regex => "^[a-z]([a-z0-9]*_?)*(_SUITE)?$",
                 ignore => []}
          },
          {
            elvis_style,
            function_naming_convention,
            #{regex => "^([a-z][a-z0-9]*_?)*$"}
             },
            {elvis_style, state_record_and_type},
            {elvis_style, no_spec_with_records}
          %% {elvis_style, dont_repeat_yourself, #{min_complexity => 10}}
          %% {elvis_style, no_debug_call, #{ignore => []}}
          ]
         },
#{dirs => ["."],
  filter => "Makefile",
  rules => [{elvis_project, no_deps_master_erlang_mk, #{ignore => []}},
            {elvis_project, protocol_for_deps_erlang_mk, #{ignore => []}}]
 },
#{dirs => ["."],
  filter => "rebar.config",
  rules => [{elvis_project, no_deps_master_rebar, #{ignore => []}},
            {elvis_project, protocol_for_deps_rebar, #{ignore => []}}]
 }
]
}.
